package com.l2jserver.gameserver.model.olympiad.tasks;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.entity.Hero;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * @author vGodFather
 */
public class OlympiadEndTask implements Runnable
{
	protected static final Logger _log = LoggerFactory.getLogger(OlympiadEndTask.class);
	
	private final List<StatsSet> _herosToBe;
	
	public OlympiadEndTask(List<StatsSet> herosToBe)
	{
		_herosToBe = herosToBe;
	}
	
	@Override
	public void run()
	{
		SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.OLYMPIAD_ROUND_S1_HAS_ENDED);
		sm.addInt(Olympiad.getInstance().getCurrentRound());
		
		Broadcast.toAllOnlinePlayers(sm);
		Broadcast.toAllOnlinePlayers("Olympiad Validation Period has began");
		_log.info("Olympiad System: Olympiad Validation Period has began");
		
		if (Olympiad.getInstance()._scheduledWeeklyTask != null)
		{
			Olympiad.getInstance()._scheduledWeeklyTask.cancel(true);
		}
		
		Olympiad.getInstance().saveNobleData();
		
		Olympiad.getInstance().setPeriod(1);
		Olympiad.getInstance().sortHerosToBe();
		Hero.getInstance().resetData();
		Hero.getInstance().computeNewHeroes(_herosToBe);
		
		Olympiad.getInstance().saveOlympiadStatus();
		Olympiad.getInstance().updateMonthlyData();
		
		Calendar validationEnd = Calendar.getInstance();
		Olympiad.getInstance();
		Olympiad.getInstance()._validationEnd = validationEnd.getTimeInMillis() + Olympiad.VALIDATION_PERIOD;
		
		Olympiad.getInstance().loadNoblesRank();
		Olympiad.getInstance()._scheduledValdationTask = ThreadPoolManager.getInstance().scheduleGeneral(new ValidationEndTask(), Olympiad.getInstance().getMillisToValidationEnd());
		
		final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		_log.info("Olympiad System: Validation Period ends at " + format.format(Olympiad.getInstance().getMillisToValidationEnd() + System.currentTimeMillis()));
	}
}