/*
 * Copyright © 2004-2019 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.skills.targets;

/**
 * @author Zoey76
 */
public enum L2TargetType
{
	area,
	area_corpse,
	area_friend,
	area_summon,
	area_undead,
	aura,
	aura_corpse,
	aura_friend,
	aura_undead,
	area_behind,
	aura_behind,
	clan,
	clan_member,
	command_channel,
	clan_corpse,
	corpse,
	enemy,
	enemy_only,
	enemy_summon,
	flag_pole,
	area_front,
	aura_front,
	ground,
	holy,
	none,
	one,
	pet_owner,
	party,
	party_clan,
	party_member,
	party_not_me,
	party_other,
	pc_body,
	pet,
	self,
	servitor,
	summon,
	target_party,
	undead,
	unlockable
}
