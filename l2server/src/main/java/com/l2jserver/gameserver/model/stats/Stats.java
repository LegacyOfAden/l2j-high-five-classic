/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats;

import java.util.NoSuchElementException;

/**
 * Enum of basic stats.
 * @author mkizub
 */
public enum Stats
{
	// Base stats, for each in Calculator a slot is allocated
	
	// EXP SP RATE
	EXPSP_RATE("expSpRate"),
	
	// HP, MP & CP
	MAX_HP("p_max_hp"),
	MAX_MP("p_max_mp"),
	MAX_CP("p_max_cp"),
	MAX_RECOVERABLE_HP("p_limit_hp"), // The maximum HP that is able to be recovered trough heals
	MAX_RECOVERABLE_MP("p_limit_mp"),
	MAX_RECOVERABLE_CP("p_limit_cp"),
	REGENERATE_HP_RATE("p_hp_regen"),
	REGENERATE_CP_RATE("p_cp_regen"),
	REGENERATE_MP_RATE("p_mp_regen"),
	MANA_CHARGE("p_mana_charge"),
	HEAL_EFFECT("p_heal_effect"),
	S_HP_PROP_HP_RATE("sNpcPropHpRate"),
	
	// ATTACK & DEFENCE
	POWER_DEFENCE("p_physical_defence"),
	MAGIC_DEFENCE("p_magical_defence"),
	POWER_ATTACK("p_physical_attack"),
	MAGIC_ATTACK("p_magical_attack"),
	PHYSICAL_SKILL_POWER("p_skill_power"),
	MAGICAL_SKILL_POWER("m_skill_power"),
	POWER_ATTACK_SPEED("p_attack_speed"),
	MAGIC_ATTACK_SPEED("p_magic_speed"), // Magic Skill Casting Time Rate
	ATK_REUSE("p_attack_reuse"), // Bows Hits Reuse Rate
	P_REUSE("p_reuse_delay_physical"), // Physical Skill Reuse Rate
	STATIC_REUSE("p_reuse_delay_static"), // Static Skill Reuse Rate
	MAGIC_REUSE("p_reuse_delay_magic"), // Magic Skill Reuse Rate
	DANCE_REUSE("p_reuse_delay_dance"), // Dance Skill Reuse Rate
	SHIELD_DEFENCE("p_physical_shield_defence"),
	CRITICAL_DAMAGE("p_critical_damage"),
	CRITICAL_DAMAGE_POS("p_critical_damage_position"),
	CRITICAL_DAMAGE_ADD("p_critical_damage_add"),
	MAGIC_CRITICAL_DAMAGE_ADD("p_magic_critical_damage_add"),
	MAGIC_CRIT_DMG("p_magic_critical_dmg"),
	
	// PVP BONUS
	PVP_PHYSICAL_DMG("p_pvp_physical_attack_dmg_bonus"),
	PVP_MAGICAL_DMG("p_pvp_magical_skill_dmg_bonus"),
	PVP_PHYS_SKILL_DMG("p_pvp_physical_skill_dmg_bonus"),
	PVP_PHYSICAL_DEF("p_pvp_physical_attack_defence_bonus"),
	PVP_MAGICAL_DEF("p_pvp_magical_skill_defence_bonus"),
	PVP_PHYS_SKILL_DEF("p_pvp_physical_skill_defence_bonus"),
	
	// PVE BONUS
	PVE_PHYSICAL_DMG("p_pve_physical_attack_dmg_bonus"),
	PVE_MAGICAL_DMG("p_pve_magical_attack_dmg_bonus"),
	PVE_PHYS_SKILL_DMG("p_pve_physical_skill_dmg_bonus"),
	PVE_PHYSICAL_DEF("p_pve_physical_attack_defence_bonus"),
	PVE_MAGICAL_DEF("p_pve_magical_skill_defence_bonus"),
	PVE_PHYS_SKILL_DEF("p_pve_physical_skill_defence_bonus"),
	
	// ATTACK & DEFENCE RATES
	EVASION_RATE("p_avoid"),
	P_SKILL_EVASION("p_avoid_skill"),
	M_SKILL_EVASION("m_avoid_skill"),
	S_SKILL_EVASION("s_avoid_skill"),
	DEFENCE_CRITICAL_RATE("p_defence_critical_rate"),
	DEFENCE_CRITICAL_RATE_ADD("p_defence_critical_rate_add"),
	DEFENCE_CRITICAL_DAMAGE("p_defence_critical_damage"),
	DEFENCE_CRITICAL_DAMAGE_ADD("p_defence_critical_damage_add"), // Resistance to critical damage in value (Example: +100 will be 100 more critical damage, NOT 100% more).
	SHIELD_RATE("p_shield_defence_rate"),
	CRITICAL_RATE("p_critical_rate"),
	CRITICAL_RATE_POS("p_critical_rate_position_bonus"),
	BLOW_RATE("p_fatal_blow_rate"),
	MAGIC_CRITICAL_RATE("p_magic_critical_rate"),
	BONUS_EXP("p_exp_add"),
	BONUS_SP("p_sp_modify"),
	P_REDUCE_CANCEL("p_reduce_cancel"),
	
	// ACCURACY & RANGE
	ACCURACY_COMBAT("p_hit"),
	POWER_ATTACK_RANGE("p_attack_range"),
	MAGIC_ATTACK_RANGE("m_attack_range"),
	ATTACK_COUNT_MAX("p_hit_number"),
	// Run speed, walk & escape speed are calculated proportionally, magic speed is a buff
	MOVE_SPEED("p_speed"),
	
	// BASIC STATS
	STAT_STR("STR"),
	STAT_CON("CON"),
	STAT_DEX("DEX"),
	STAT_INT("INT"),
	STAT_WIT("WIT"),
	STAT_MEN("MEN"),
	
	// Special stats, share one slot in Calculator
	
	// VARIOUS
	BREATH("p_breath"),
	FALL("p_safe_fall_height"),
	
	// VULNERABILITIES
	DAMAGE_ZONE_VULN("p_area_damage"),
	RESIST_SPEED_DOWN("speed_down"),
	RESIST_DISPEL_BUFF("slot_buff"),
	RESIST_ABNORMAL_DEBUFF("slot_debuff"),
	RESIST_ABNORMAL_BUFF("buffVuln"),
	
	// RESISTANCES
	FIRE_RES("fireRes"),
	WIND_RES("windRes"),
	WATER_RES("waterRes"),
	EARTH_RES("earthRes"),
	HOLY_RES("holyRes"),
	DARK_RES("darkRes"),
	MAGIC_SUCCESS_RES("p_resist_dd_magic"),
	MAGIC_FAILURE_RATE("p_magic_failure_rate"),
	
	// ELEMENT POWER
	FIRE_POWER("firePower"),
	WATER_POWER("waterPower"),
	WIND_POWER("windPower"),
	EARTH_POWER("earthPower"),
	HOLY_POWER("holyPower"),
	DARK_POWER("darkPower"),
	
	// PROFICIENCY
	CANCEL_PROF("cancelProf"),
	
	REFLECT_DAMAGE_PERCENT("p_damage_shield"),
	REFLECT_SKILL_MAGIC("p_reflect_skill_magical"),
	REFLECT_SKILL_PHYSIC("p_reflect_skill_physical"),
	REFLECT_SKILL_STATIC("p_reflect_skill_static"),
	VENGEANCE_SKILL_MAGIC_DAMAGE("p_reflect_dd"),
	VENGEANCE_SKILL_PHYSICAL_DAMAGE("p_counter_skill"),
	VENGEANCE_SKILL_MAGICAL_DAMAGE("m_counter_skill"),
	VENGEANCE_SKILL_STATIC_DAMAGE("s_counter_skill"),
	ABSORB_DAMAGE_PERCENT_CHANCE("p_vampiric_attack_chance"),
	ABSORB_DAMAGE_PERCENT("p_vampiric_attack"),
	P_TRANSFER_DAMAGE_TO_SUMMON("p_transfer_damage_summon"),
	MANA_SHIELD_PERCENT("p_mp_shield"),
	TRANSFER_DAMAGE_TO_PLAYER("p_transfer_damage_pc"),
	ABSORB_MANA_DAMAGE_PERCENT("p_mp_vampiric_attack"),
	
	WEIGHT_LIMIT("p_weight_limit"),
	WEIGHT_PENALTY("p_weight_penalty"),
	ENLARGE_ABNORMAL_SLOT("enlargeAbnormalSlot"),
	
	// ExSkill
	INV_LIM("inventory_normal"),
	WH_LIM("storage_private_warehouse"),
	FREIGHT_LIM("storage_private_freight"),
	P_SELL_LIM("trade_sell"),
	P_BUY_LIM("trade_buy"),
	REC_D_LIM("recipe_dwarven"),
	REC_C_LIM("recipe_common"),
	
	// C4 Stats
	PHYSICAL_MP_CONSUME("p_physical_mp_cost"),
	MAGICAL_MP_CONSUME("p_magic_mp_cost"),
	DANCE_MP_CONSUME("p_dance_mp_cost"),
	STATIC_MP_CONSUME("p_static_mp_cost"),
	BOW_MP_CONSUME_RATE("p_cheapshot"),
	MP_CONSUME("p_mp_consume"),
	
	// Shield Stats
	SHIELD_DEFENCE_ANGLE("p_physical_shield_defence_angle_all"),
	
	// Skill mastery
	SKILL_CRITICAL("p_skill_critical"),
	SKILL_CRITICAL_PROBABILITY("p_skill_critical_probability"),
	
	// Vitality
	VITALITY_CONSUME_RATE("vitalityConsumeRate"),
	RECHARGE_VITALITY("vitalityChange"),
	
	// Souls
	MAX_SOULS("p_max_soul"),
	
	REDUCE_EXP_LOST_BY_PK("p_reduce_exp_penalty_pk"),
	REDUCE_EXP_LOST_BY_MOB("p_reduce_exp_penalty_mob"),
	REDUCE_EXP_LOST_BY_RAID("p_reduce_exp_penalty_raid"),
	
	REDUCE_DEATH_PENALTY_BY_PK("p_reduce_drop_penalty_pk"),
	REDUCE_DEATH_PENALTY_BY_MOB("p_reduce_drop_penalty_mob"),
	REDUCE_DEATH_PENALTY_BY_RAID("p_reduce_drop_penalty_raid"),
	
	// Fishing
	FISHING_EXPERTISE("p_fishing_mastery");
	
	public static final int NUM_STATS = values().length;
	
	private String _value;
	
	public String getValue()
	{
		return _value;
	}
	
	private Stats(String s)
	{
		_value = s;
	}
	
	public static Stats valueOfXml(String name)
	{
		name = name.intern();
		for (Stats s : values())
		{
			if (s.getValue().equals(name))
			{
				return s;
			}
		}
		
		throw new NoSuchElementException("Unknown name '" + name + "' for enum BaseStats");
	}
}
