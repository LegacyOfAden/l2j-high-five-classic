/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.tasks.character;

import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Task dedicated to queued magic use of character
 * @author xban1x
 */
public final class QueuedSummonMagicUseTask implements Runnable
{
	private final L2Summon _currSummon;
	private final Skill _queuedSkill;
	private final boolean _isCtrlPressed;
	private final boolean _isShiftPressed;
	
	public QueuedSummonMagicUseTask(L2Summon currSummon, Skill queuedSkill, boolean isCtrlPressed, boolean isShiftPressed)
	{
		_currSummon = currSummon;
		_queuedSkill = queuedSkill;
		_isCtrlPressed = isCtrlPressed;
		_isShiftPressed = isShiftPressed;
	}
	
	@Override
	public void run()
	{
		if (_currSummon != null)
		{
			_currSummon.useMagic(_queuedSkill, _isCtrlPressed, _isShiftPressed);
		}
	}
}
