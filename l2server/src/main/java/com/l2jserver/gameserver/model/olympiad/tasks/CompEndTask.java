package com.l2jserver.gameserver.model.olympiad.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.model.olympiad.OlympiadGameManager;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * @author vGodFather
 */
public class CompEndTask implements Runnable
{
	private static final Logger _log = LoggerFactory.getLogger(CompEndTask.class);
	
	@Override
	public void run()
	{
		if (Olympiad.getInstance().isOlympiadEnd())
		{
			return;
		}
		
		Olympiad.getInstance()._inCompPeriod = false;
		
		// if there are games left, wait for them to finish one more minute
		if (OlympiadGameManager.getInstance().isBattleStarted())
		{
			ThreadPoolManager.getInstance().scheduleGeneral(new CompEndTask(), 60000);
			return;
		}
		
		Broadcast.toAllOnlinePlayers(SystemMessage.getSystemMessage(SystemMessageId.THE_OLYMPIAD_GAME_HAS_ENDED));
		_log.info("Olympiad System: Olympiad Game Ended");
		
		if (Olympiad.getInstance()._gameManagerTask != null)
		{
			Olympiad.getInstance()._gameManagerTask.cancel(false);
			Olympiad.getInstance()._gameManagerTask = null;
		}
		
		Olympiad.getInstance().saveOlympiadStatus();
		
		Olympiad.getInstance().init();
	}
}