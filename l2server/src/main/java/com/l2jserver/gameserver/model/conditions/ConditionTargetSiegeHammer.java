package com.l2jserver.gameserver.model.conditions;

import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2DoorInstance;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * The Class ConditionTargetSiegeHammer.
 * @author vGodFather
 */
public class ConditionTargetSiegeHammer extends Condition
{
	public ConditionTargetSiegeHammer()
	{
		
	}
	
	@Override
	public boolean testImpl(L2Character effector, L2Character effected, Skill skill, L2Item item)
	{
		return (effected.getTarget() instanceof L2DoorInstance);
	}
}
