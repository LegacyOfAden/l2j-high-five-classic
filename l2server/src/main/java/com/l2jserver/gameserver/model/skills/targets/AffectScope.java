/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.skills.targets;

/**
 * Affect scope enumerated.
 * @author Zoey76
 */
public enum AffectScope
{
	/** Affects Valakas. */
	balakas_scope,
	/** Affects dead clan mates. */
	dead_pledge,
	/** Affects dead union (Command Channel?) members. */
	dead_union,
	/** Affects fan area. */
	fan,
	/** Affects fan area, using caster as point of origin.. */
	fan_pb,
	/** Affects nothing. */
	none,
	/** Affects party members. */
	party,
	/** Affects dead party members. */
	dead_party,
	/** Affects party and clan mates. */
	party_pledge,
	/** Affects dead party and clan members. */
	dead_party_pledge,
	/** Affects clan mates. */
	pledge,
	/** Affects point blank targets, using caster as point of origin. */
	point_blank,
	/** Affects ranged targets, using selected target as point of origin. */
	range,
	/** Affects ranged targets, using selected target as point of origin sorted by lowest to highest HP. */
	range_sort_by_hp,
	/** Affects targets in donut shaped area, using caster as point of origin. */
	ring_range,
	/** Affects a single target. */
	single,
	/** Affects targets inside an square area, using selected target as point of origin. */
	square,
	/** Affects targets inside an square area, using caster as point of origin. */
	square_pb,
	/** Affects static object targets. */
	static_object_scope,
	/** Affects all summons except master. */
	summon_except_master,
	/** Affects wyverns. */
	wyvern_scope
}
