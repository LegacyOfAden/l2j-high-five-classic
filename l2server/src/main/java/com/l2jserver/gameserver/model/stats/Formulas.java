/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.l2jserver.Config;
import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.SevenSignsFestival;
import com.l2jserver.gameserver.cubic.CubicInstance;
import com.l2jserver.gameserver.data.xml.impl.HitConditionBonusData;
import com.l2jserver.gameserver.data.xml.impl.KarmaData;
import com.l2jserver.gameserver.enums.DispelCategory;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.instancemanager.SiegeManager;
import com.l2jserver.gameserver.instancemanager.ZoneManager;
import com.l2jserver.gameserver.model.L2SiegeClan;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PetInstance;
import com.l2jserver.gameserver.model.actor.instance.L2SiegeFlagInstance;
import com.l2jserver.gameserver.model.actor.instance.L2StaticObjectInstance;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.ClanHall;
import com.l2jserver.gameserver.model.entity.Fort;
import com.l2jserver.gameserver.model.entity.Siege;
import com.l2jserver.gameserver.model.items.L2Armor;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.ArmorType;
import com.l2jserver.gameserver.model.items.type.WeaponType;
import com.l2jserver.gameserver.model.skills.AbnormalType;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncArmorSet;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncAtkAccuracy;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncAtkCritical;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncAtkEvasion;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncGatesMDefMod;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncGatesPDefMod;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncHenna;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMAtkCritical;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMAtkMod;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMAtkSpeed;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMDefMod;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMaxCpMul;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMaxHpMul;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMaxMpMul;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncMoveSpeed;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncPAtkMod;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncPAtkSpeed;
import com.l2jserver.gameserver.model.stats.functions.formulas.FuncPDefMod;
import com.l2jserver.gameserver.model.zone.ZoneId;
import com.l2jserver.gameserver.model.zone.type.L2CastleZone;
import com.l2jserver.gameserver.model.zone.type.L2ClanHallZone;
import com.l2jserver.gameserver.model.zone.type.L2FortZone;
import com.l2jserver.gameserver.model.zone.type.L2MotherTreeZone;
import com.l2jserver.gameserver.network.Debug;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Util;
import com.l2jserver.util.Rnd;

/**
 * Global calculations.
 */
public final class Formulas
{
	private static final Logger _log = Logger.getLogger(Formulas.class.getName());
	
	/** Regeneration Task period. */
	private static final int HP_REGENERATE_PERIOD = 3000; // 3 secs
	
	public static final byte SHIELD_DEFENSE_FAILED = 0; // no shield defense
	public static final byte SHIELD_DEFENSE_SUCCEED = 1; // normal shield defense
	public static final byte SHIELD_DEFENSE_PERFECT_BLOCK = 2; // perfect block
	
	/**
	 * Return the period between 2 regeneration task (3s for L2Character, 5 min for L2DoorInstance).
	 * @param cha
	 * @return
	 */
	public static int getRegeneratePeriod(L2Character cha)
	{
		return cha.isDoor() ? HP_REGENERATE_PERIOD * 100 : HP_REGENERATE_PERIOD;
	}
	
	/**
	 * Return the standard NPC Calculator set containing ACCURACY_COMBAT and EVASION_RATE.<br>
	 * <B><U>Concept</U>:</B><br>
	 * A calculator is created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...). In fact, each calculator is a table of Func object in which each Func represents a mathematic function : <br>
	 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<br>
	 * To reduce cache memory use, L2NPCInstances who don't have skills share the same Calculator set called <B>NPC_STD_CALCULATOR</B>.<br>
	 * @return
	 */
	public static Calculator[] getStdNPCCalculators()
	{
		Calculator[] std = new Calculator[Stats.NUM_STATS];
		
		std[Stats.MAX_HP.ordinal()] = new Calculator();
		std[Stats.MAX_HP.ordinal()].addFunc(FuncMaxHpMul.getInstance());
		
		std[Stats.MAX_MP.ordinal()] = new Calculator();
		std[Stats.MAX_MP.ordinal()].addFunc(FuncMaxMpMul.getInstance());
		
		std[Stats.POWER_ATTACK.ordinal()] = new Calculator();
		std[Stats.POWER_ATTACK.ordinal()].addFunc(FuncPAtkMod.getInstance());
		
		std[Stats.MAGIC_ATTACK.ordinal()] = new Calculator();
		std[Stats.MAGIC_ATTACK.ordinal()].addFunc(FuncMAtkMod.getInstance());
		
		std[Stats.POWER_DEFENCE.ordinal()] = new Calculator();
		std[Stats.POWER_DEFENCE.ordinal()].addFunc(FuncPDefMod.getInstance());
		
		std[Stats.MAGIC_DEFENCE.ordinal()] = new Calculator();
		std[Stats.MAGIC_DEFENCE.ordinal()].addFunc(FuncMDefMod.getInstance());
		
		std[Stats.CRITICAL_RATE.ordinal()] = new Calculator();
		std[Stats.CRITICAL_RATE.ordinal()].addFunc(FuncAtkCritical.getInstance());
		
		std[Stats.MAGIC_CRITICAL_RATE.ordinal()] = new Calculator();
		std[Stats.MAGIC_CRITICAL_RATE.ordinal()].addFunc(FuncMAtkCritical.getInstance());
		
		std[Stats.ACCURACY_COMBAT.ordinal()] = new Calculator();
		std[Stats.ACCURACY_COMBAT.ordinal()].addFunc(FuncAtkAccuracy.getInstance());
		
		std[Stats.EVASION_RATE.ordinal()] = new Calculator();
		std[Stats.EVASION_RATE.ordinal()].addFunc(FuncAtkEvasion.getInstance());
		
		std[Stats.POWER_ATTACK_SPEED.ordinal()] = new Calculator();
		std[Stats.POWER_ATTACK_SPEED.ordinal()].addFunc(FuncPAtkSpeed.getInstance());
		
		std[Stats.MAGIC_ATTACK_SPEED.ordinal()] = new Calculator();
		std[Stats.MAGIC_ATTACK_SPEED.ordinal()].addFunc(FuncMAtkSpeed.getInstance());
		
		std[Stats.MOVE_SPEED.ordinal()] = new Calculator();
		std[Stats.MOVE_SPEED.ordinal()].addFunc(FuncMoveSpeed.getInstance());
		
		return std;
	}
	
	public static Calculator[] getStdDoorCalculators()
	{
		Calculator[] std = new Calculator[Stats.NUM_STATS];
		
		// Add the FuncAtkAccuracy to the Standard Calculator of ACCURACY_COMBAT
		std[Stats.ACCURACY_COMBAT.ordinal()] = new Calculator();
		std[Stats.ACCURACY_COMBAT.ordinal()].addFunc(FuncAtkAccuracy.getInstance());
		
		// Add the FuncAtkEvasion to the Standard Calculator of EVASION_RATE
		std[Stats.EVASION_RATE.ordinal()] = new Calculator();
		std[Stats.EVASION_RATE.ordinal()].addFunc(FuncAtkEvasion.getInstance());
		
		// SevenSigns PDEF Modifier
		std[Stats.POWER_DEFENCE.ordinal()] = new Calculator();
		std[Stats.POWER_DEFENCE.ordinal()].addFunc(FuncGatesPDefMod.getInstance());
		
		// SevenSigns MDEF Modifier
		std[Stats.MAGIC_DEFENCE.ordinal()] = new Calculator();
		std[Stats.MAGIC_DEFENCE.ordinal()].addFunc(FuncGatesMDefMod.getInstance());
		
		return std;
	}
	
	/**
	 * Add basics stats functions to a player.<br>
	 * <B><U>Concept</U>:</B><br>
	 * A calculator is created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...).<br>
	 * In fact, each calculator is a table of Func object in which each Func represents a mathematics function: <br>
	 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<br>
	 * @param player the player
	 */
	public static void addFuncsToNewPlayer(L2PcInstance player)
	{
		player.addStatFunc(FuncMaxHpMul.getInstance());
		player.addStatFunc(FuncMaxCpMul.getInstance());
		player.addStatFunc(FuncMaxMpMul.getInstance());
		player.addStatFunc(FuncPAtkMod.getInstance());
		player.addStatFunc(FuncMAtkMod.getInstance());
		player.addStatFunc(FuncPDefMod.getInstance());
		player.addStatFunc(FuncMDefMod.getInstance());
		player.addStatFunc(FuncAtkCritical.getInstance());
		player.addStatFunc(FuncMAtkCritical.getInstance());
		player.addStatFunc(FuncAtkAccuracy.getInstance());
		player.addStatFunc(FuncAtkEvasion.getInstance());
		player.addStatFunc(FuncPAtkSpeed.getInstance());
		player.addStatFunc(FuncMAtkSpeed.getInstance());
		player.addStatFunc(FuncMoveSpeed.getInstance());
		
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_STR));
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_DEX));
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_INT));
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_MEN));
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_CON));
		player.addStatFunc(FuncHenna.getInstance(Stats.STAT_WIT));
		
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_STR));
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_DEX));
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_INT));
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_MEN));
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_CON));
		player.addStatFunc(FuncArmorSet.getInstance(Stats.STAT_WIT));
	}
	
	public static void addFuncsToNewSummon(L2Summon summon)
	{
		summon.addStatFunc(FuncMaxHpMul.getInstance());
		summon.addStatFunc(FuncMaxMpMul.getInstance());
		summon.addStatFunc(FuncPAtkMod.getInstance());
		summon.addStatFunc(FuncMAtkMod.getInstance());
		summon.addStatFunc(FuncPDefMod.getInstance());
		summon.addStatFunc(FuncMDefMod.getInstance());
		summon.addStatFunc(FuncAtkCritical.getInstance());
		summon.addStatFunc(FuncMAtkCritical.getInstance());
		summon.addStatFunc(FuncAtkAccuracy.getInstance());
		summon.addStatFunc(FuncAtkEvasion.getInstance());
		summon.addStatFunc(FuncMoveSpeed.getInstance());
		summon.addStatFunc(FuncPAtkSpeed.getInstance());
		summon.addStatFunc(FuncMAtkSpeed.getInstance());
	}
	
	/**
	 * Calculate the HP regen rate (base + modifiers).
	 * @param cha
	 * @return
	 */
	public static final double calcHpRegen(L2Character cha)
	{
		double baseValue = cha.isPlayer() ? cha.getActingPlayer().getTemplate().getBaseHpRegen(cha.getLevel()) : cha.getTemplate().getBaseHpReg();
		baseValue *= cha.isRaid() ? Config.RAID_HP_REGEN_MULTIPLIER : Config.HP_REGEN_MULTIPLIER;
		baseValue *= cha.calcStat(Stats.S_HP_PROP_HP_RATE, cha.getTemplate().getNpcPropHpRate());
		
		if (Config.L2JMOD_CHAMPION_ENABLE && cha.isChampion())
		{
			baseValue *= Config.L2JMOD_CHAMPION_HP_REGEN;
		}
		
		if (cha.isPlayer())
		{
			L2PcInstance player = cha.getActingPlayer();
			
			// SevenSigns Festival modifier
			if (SevenSignsFestival.getInstance().isFestivalInProgress() && player.isFestivalParticipant())
			{
				baseValue *= calcFestivalRegenModifier(player);
			}
			else
			{
				double siegeModifier = calcSiegeRegenModifier(player);
				if (siegeModifier > 0)
				{
					baseValue *= siegeModifier;
				}
			}
			
			if (player.isInsideZone(ZoneId.CLAN_HALL) && (player.getClan() != null) && (player.getClan().getHideoutId() > 0))
			{
				L2ClanHallZone zone = ZoneManager.getInstance().getZone(player, L2ClanHallZone.class);
				int posChIndex = zone == null ? -1 : zone.getResidenceId();
				int clanHallIndex = player.getClan().getHideoutId();
				if ((clanHallIndex > 0) && (clanHallIndex == posChIndex))
				{
					ClanHall clansHall = ClanHallManager.getInstance().getClanHallById(clanHallIndex);
					if (clansHall != null)
					{
						if (clansHall.getFunction(ClanHall.FUNC_RESTORE_HP) != null)
						{
							baseValue *= 1 + ((double) clansHall.getFunction(ClanHall.FUNC_RESTORE_HP).getLvl() / 100);
						}
					}
				}
			}
			
			if (player.isInsideZone(ZoneId.CASTLE) && (player.getClan() != null) && (player.getClan().getCastleId() > 0))
			{
				L2CastleZone zone = ZoneManager.getInstance().getZone(player, L2CastleZone.class);
				int posCastleIndex = zone == null ? -1 : zone.getResidenceId();
				int castleIndex = player.getClan().getCastleId();
				if ((castleIndex > 0) && (castleIndex == posCastleIndex))
				{
					Castle castle = CastleManager.getInstance().getCastleById(castleIndex);
					if (castle != null)
					{
						if (castle.getFunction(Castle.FUNC_RESTORE_HP) != null)
						{
							baseValue *= 1 + ((double) castle.getFunction(Castle.FUNC_RESTORE_HP).getLvl() / 100);
						}
					}
				}
			}
			
			if (player.isInsideZone(ZoneId.FORT) && (player.getClan() != null) && (player.getClan().getFortId() > 0))
			{
				L2FortZone zone = ZoneManager.getInstance().getZone(player, L2FortZone.class);
				int posFortIndex = zone == null ? -1 : zone.getResidenceId();
				int fortIndex = player.getClan().getFortId();
				if ((fortIndex > 0) && (fortIndex == posFortIndex))
				{
					Fort fort = FortManager.getInstance().getFortById(fortIndex);
					if (fort != null)
					{
						if (fort.getFunction(Fort.FUNC_RESTORE_HP) != null)
						{
							baseValue *= 1 + ((double) fort.getFunction(Fort.FUNC_RESTORE_HP).getLvl() / 100);
						}
					}
				}
			}
			
			// Mother Tree effect is calculated at last
			if (player.isInsideZone(ZoneId.MOTHER_TREE))
			{
				L2MotherTreeZone zone = ZoneManager.getInstance().getZone(player, L2MotherTreeZone.class);
				int hpBonus = zone == null ? 0 : zone.getHpRegenBonus();
				baseValue += hpBonus;
			}
			
			// Calculate Movement bonus
			if (player.isSitting())
			{
				baseValue *= 1.5; // Sitting
			}
			else if (!player.isMoving())
			{
				baseValue *= 1.1; // Staying
			}
			else if (player.isRunning())
			{
				baseValue *= 0.7; // Running
			}
			
			// Add CON bonus
			baseValue *= cha.getLevelMod() * BaseStats.CON.calcBonus(cha);
		}
		else if (cha.isPet())
		{
			baseValue = ((L2PetInstance) cha).getPetLevelData().getPetRegenHP() * Config.PET_HP_REGEN_MULTIPLIER;
		}
		
		return (cha.calcStat(Stats.REGENERATE_HP_RATE, Math.max(1, baseValue), null, null));
	}
	
	/**
	 * Calculate the MP regen rate (base + modifiers).
	 * @param cha
	 * @return
	 */
	public static final double calcMpRegen(L2Character cha)
	{
		double baseValue = cha.isPlayer() ? cha.getActingPlayer().getTemplate().getBaseMpRegen(cha.getLevel()) : cha.getTemplate().getBaseMpReg();
		baseValue *= cha.isRaid() ? Config.RAID_MP_REGEN_MULTIPLIER : Config.MP_REGEN_MULTIPLIER;
		
		if (cha.isPlayer())
		{
			L2PcInstance player = cha.getActingPlayer();
			
			// SevenSigns Festival modifier
			if (SevenSignsFestival.getInstance().isFestivalInProgress() && player.isFestivalParticipant())
			{
				baseValue *= calcFestivalRegenModifier(player);
			}
			
			// Mother Tree effect is calculated at last'
			if (player.isInsideZone(ZoneId.MOTHER_TREE))
			{
				L2MotherTreeZone zone = ZoneManager.getInstance().getZone(player, L2MotherTreeZone.class);
				int mpBonus = zone == null ? 0 : zone.getMpRegenBonus();
				baseValue += mpBonus;
			}
			
			if (player.isInsideZone(ZoneId.CLAN_HALL) && (player.getClan() != null) && (player.getClan().getHideoutId() > 0))
			{
				L2ClanHallZone zone = ZoneManager.getInstance().getZone(player, L2ClanHallZone.class);
				int posChIndex = zone == null ? -1 : zone.getResidenceId();
				int clanHallIndex = player.getClan().getHideoutId();
				if ((clanHallIndex > 0) && (clanHallIndex == posChIndex))
				{
					ClanHall clansHall = ClanHallManager.getInstance().getClanHallById(clanHallIndex);
					if (clansHall != null)
					{
						if (clansHall.getFunction(ClanHall.FUNC_RESTORE_MP) != null)
						{
							baseValue *= 1 + ((double) clansHall.getFunction(ClanHall.FUNC_RESTORE_MP).getLvl() / 100);
						}
					}
				}
			}
			
			if (player.isInsideZone(ZoneId.CASTLE) && (player.getClan() != null) && (player.getClan().getCastleId() > 0))
			{
				L2CastleZone zone = ZoneManager.getInstance().getZone(player, L2CastleZone.class);
				int posCastleIndex = zone == null ? -1 : zone.getResidenceId();
				int castleIndex = player.getClan().getCastleId();
				if ((castleIndex > 0) && (castleIndex == posCastleIndex))
				{
					Castle castle = CastleManager.getInstance().getCastleById(castleIndex);
					if (castle != null)
					{
						if (castle.getFunction(Castle.FUNC_RESTORE_MP) != null)
						{
							baseValue *= 1 + ((double) castle.getFunction(Castle.FUNC_RESTORE_MP).getLvl() / 100);
						}
					}
				}
			}
			
			if (player.isInsideZone(ZoneId.FORT) && (player.getClan() != null) && (player.getClan().getFortId() > 0))
			{
				L2FortZone zone = ZoneManager.getInstance().getZone(player, L2FortZone.class);
				int posFortIndex = zone == null ? -1 : zone.getResidenceId();
				int fortIndex = player.getClan().getFortId();
				if ((fortIndex > 0) && (fortIndex == posFortIndex))
				{
					Fort fort = FortManager.getInstance().getFortById(fortIndex);
					if (fort != null)
					{
						if (fort.getFunction(Fort.FUNC_RESTORE_MP) != null)
						{
							baseValue *= 1 + ((double) fort.getFunction(Fort.FUNC_RESTORE_MP).getLvl() / 100);
						}
					}
				}
			}
			
			// Calculate Movement bonus
			if (player.isSitting())
			{
				baseValue *= 1.5; // Sitting
			}
			else if (!player.isMoving())
			{
				baseValue *= 1.1; // Staying
			}
			else if (player.isRunning())
			{
				baseValue *= 0.7; // Running
			}
			
			// Add MEN bonus
			baseValue *= cha.getLevelMod() * BaseStats.MEN.calcBonus(cha);
		}
		else if (cha.isPet())
		{
			baseValue = ((L2PetInstance) cha).getPetLevelData().getPetRegenMP() * Config.PET_MP_REGEN_MULTIPLIER;
		}
		
		return (cha.calcStat(Stats.REGENERATE_MP_RATE, Math.max(1, baseValue), null, null));
	}
	
	/**
	 * Calculates the CP regeneration rate (base + modifiers).
	 * @param player the player
	 * @return the CP regeneration rate
	 */
	public static final double calcCpRegen(L2PcInstance player)
	{
		// With CON bonus
		double baseValue = player.getActingPlayer().getTemplate().getBaseCpRegen(player.getLevel()) * player.getLevelMod() * BaseStats.CON.calcBonus(player);
		baseValue *= Config.CP_REGEN_MULTIPLIER;
		if (player.isSitting())
		{
			baseValue *= 1.5; // Sitting
		}
		else if (!player.isMoving())
		{
			baseValue *= 1.1; // Staying
		}
		else if (player.isRunning())
		{
			baseValue *= 0.7; // Running
		}
		return player.calcStat(Stats.REGENERATE_CP_RATE, Math.max(1, baseValue), null, null);
	}
	
	public static final double calcFestivalRegenModifier(L2PcInstance activeChar)
	{
		final int[] festivalInfo = SevenSignsFestival.getInstance().getFestivalForPlayer(activeChar);
		final int oracle = festivalInfo[0];
		final int festivalId = festivalInfo[1];
		int[] festivalCenter;
		
		// If the player isn't found in the festival, leave the regen rate as it is.
		if (festivalId < 0)
		{
			return 0;
		}
		
		// Retrieve the X and Y coords for the center of the festival arena the player is in.
		if (oracle == SevenSigns.CABAL_DAWN)
		{
			festivalCenter = SevenSignsFestival.FESTIVAL_DAWN_PLAYER_SPAWNS[festivalId];
		}
		else
		{
			festivalCenter = SevenSignsFestival.FESTIVAL_DUSK_PLAYER_SPAWNS[festivalId];
		}
		
		// Check the distance between the player and the player spawn point, in the center of the arena.
		double distToCenter = activeChar.calculateDistance(festivalCenter[0], festivalCenter[1], 0, false, false);
		
		if (Config.DEBUG)
		{
			_log.info("Distance: " + distToCenter + ", RegenMulti: " + ((distToCenter * 2.5) / 50));
		}
		
		return 1.0 - (distToCenter * 0.0005); // Maximum Decreased Regen of ~ -65%;
	}
	
	public static final double calcSiegeRegenModifier(L2PcInstance activeChar)
	{
		if ((activeChar == null) || (activeChar.getClan() == null))
		{
			return 0;
		}
		
		Siege siege = SiegeManager.getInstance().getSiege(activeChar.getX(), activeChar.getY(), activeChar.getZ());
		if ((siege == null) || !siege.isInProgress())
		{
			return 0;
		}
		
		L2SiegeClan siegeClan = siege.getAttackerClan(activeChar.getClan().getId());
		if ((siegeClan == null) || siegeClan.getFlag().isEmpty() || !Util.checkIfInRange(200, activeChar, siegeClan.getFlag().get(0), true))
		{
			return 0;
		}
		
		return 1.5; // If all is true, then modifier will be 50% more
	}
	
	public static double calcBlowDamage(L2Character attacker, L2Character target, Skill skill, double pAtk, byte shield, boolean crit, boolean ss, double power)
	{
		double pDef = target.getPDef(attacker);
		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		final boolean isPvE = (attacker.isPlayable() && target.isMonster()) || (attacker.isMonster() && target.isPlayable());
		double proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.05;
		double randomMod = attacker.getRandomDamageMultiplier();
		L2Weapon weaponItem = attacker.getActiveWeaponItem();
		boolean dual = ((weaponItem != null) && weaponItem.isDual());
		double shotsBonus = ss && dual ? 2.04 : ss ? 2 : 1;
		double pvpBonus = 1;
		double pveBonus = 1;
		
		if (isPvP)
		{
			// Damage bonuses in PvP fight
			pvpBonus = attacker.calcStat(Stats.PVP_PHYS_SKILL_DMG, 1, null, null);
			// Defense bonuses in PvP fight
			pDef *= target.calcStat(Stats.PVP_PHYS_SKILL_DEF, 1, null, null);
		}
		
		if (isPvE)
		{
			// Damage bonuses in PvP fight
			pveBonus = attacker.calcStat(Stats.PVE_PHYS_SKILL_DMG, 1, null, null);
			// Defense bonuses in PvP fight
			pDef *= target.calcStat(Stats.PVE_PHYS_SKILL_DEF, 1, null, null);
		}
		
		// Initial damage
		double baseMod = (77.0791 * (power + pAtk) * proximityBonus * randomMod) / pDef;
		// Critical
		double criticalMod = (attacker.calcStat(Stats.CRITICAL_DAMAGE, 1, target, skill));
		double criticalModPos = (((attacker.calcStat(Stats.CRITICAL_DAMAGE_POS, 1, target, skill) - 1) / 2) + 1);
		double criticalVulnMod = (((target.calcStat(Stats.DEFENCE_CRITICAL_DAMAGE, 1, target, skill) - 1) / 2) + 1);
		double criticalAddMod = ((attacker.calcStat(Stats.CRITICAL_DAMAGE_ADD, 0, target, skill) * 6.1 * 77) / pDef);
		double criticalAddVuln = ((target.calcStat(Stats.DEFENCE_CRITICAL_DAMAGE_ADD, 0, target, skill) * 6.1 * 77) / pDef);
		// Trait, elements
		double weaponTraitMod = calcWeaponTraitBonus(attacker, target);
		double generalTraitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true);
		double attributeMod = calcAttributeBonus(attacker, target, skill);
		double penaltyMod = 1;
		
		if ((target instanceof L2Attackable) && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) >= 2))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			if (lvlDiff >= Config.NPC_SKILL_DMG_PENALTY.size())
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(Config.NPC_SKILL_DMG_PENALTY.size() - 1);
			}
			else
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(lvlDiff);
			}
		}
		
		double damage = (baseMod * shotsBonus * criticalMod * criticalModPos * criticalVulnMod * pvpBonus * pveBonus) + criticalAddMod + criticalAddVuln;
		damage *= weaponTraitMod;
		damage *= generalTraitMod;
		damage *= attributeMod;
		damage *= penaltyMod;
		damage = attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage);
		
		switch (shield)
		{
			case SHIELD_DEFENSE_SUCCEED:
			{
				pDef += target.getShldDef();
				break;
			}
			case SHIELD_DEFENSE_PERFECT_BLOCK:
			{
				damage = 1;
				break;
			}
		}
		
		if (attacker.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("skillPower", power);
			set.set("shotsBonus", shotsBonus);
			set.set("randomMod", randomMod);
			set.set("proximityBonus", proximityBonus);
			set.set("pvpBonus", pvpBonus);
			set.set("baseMod", baseMod);
			set.set("criticalMod", criticalMod);
			set.set("criticalModPos", criticalModPos);
			set.set("criticalVulnMod", criticalVulnMod);
			set.set("criticalAddMod", criticalAddMod);
			set.set("criticalAddVuln", criticalAddVuln);
			set.set("weaponTraitMod", weaponTraitMod);
			set.set("generalTraitMod", generalTraitMod);
			set.set("attributeMod", attributeMod);
			set.set("pSkillPower", attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage));
			set.set("penaltyMod", penaltyMod);
			set.set("damage", (int) damage);
			Debug.sendSkillDebug(attacker, target, skill, set);
		}
		return Math.max(damage, 0.5);
	}
	
	/**
	 * Calculated damage caused by ATTACK of attacker on target.
	 * @param attacker player or NPC that makes ATTACK
	 * @param target player or NPC, target of ATTACK
	 * @param shld
	 * @param crit if the ATTACK have critical success
	 * @param ss if weapon item was charged by soulshot
	 * @return
	 */
	public static final double calcAutoAttackDamage(L2Character attacker, L2Character target, byte shield, boolean crit, boolean ss)
	{
		double pDef = target.getPDef(attacker);
		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		final boolean isPvE = (attacker.isPlayable() && target.isMonster()) || (attacker.isMonster() && target.isPlayable());
		double proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.05;
		double pAtk = attacker.getPAtk(target);
		L2Weapon weaponItem = attacker.getActiveWeaponItem();
		boolean dual = ((weaponItem != null) && weaponItem.isDual());
		double accuracyBonus = 1 + Math.max((((target.getEvasionRate(attacker) - attacker.getAccuracy()) / 100) / 2), 0);
		double shotsBonus = ss && dual ? 2.04 : ss ? 2 : 1;
		double randomMod = attacker.getRandomDamageMultiplier();
		// Trait, elements
		double attackTraitMod = calcAttackTraitBonus(attacker, target);
		double attributeMod = calcAttributeBonus(attacker, target, null);
		
		// Defense bonuses in PvP fight
		if (isPvP)
		{
			pDef *= target.calcStat(Stats.PVP_PHYSICAL_DEF, 1, null, null);
		}
		
		// Defense bonuses in PvE fight
		if (isPvE)
		{
			pDef *= target.calcStat(Stats.PVE_PHYSICAL_DEF, 1, null, null);
		}
		
		// Initial damage
		double baseMod = (77.0791 * pAtk * accuracyBonus * proximityBonus * randomMod) / pDef;
		
		double damage = baseMod * shotsBonus * attackTraitMod * attributeMod;
		
		if (crit)
		{
			damage *= 2 * attacker.calcStat(Stats.CRITICAL_DAMAGE, 1, target, null) * attacker.calcStat(Stats.CRITICAL_DAMAGE_POS, 1, target, null) * target.calcStat(Stats.DEFENCE_CRITICAL_DAMAGE, 1, target, null);
			damage += ((attacker.calcStat(Stats.CRITICAL_DAMAGE_ADD, 0, target, null) * 6.1 * 77) / pDef);
			damage += ((target.calcStat(Stats.DEFENCE_CRITICAL_DAMAGE_ADD, 0, target, null) * 6.1 * 77) / pDef);
		}
		
		if (isPvP)
		{
			damage *= attacker.calcStat(Stats.PVP_PHYSICAL_DMG, 1, null, null);
		}
		
		if (isPvE)
		{
			damage *= attacker.calcStat(Stats.PVE_PHYSICAL_DMG, 1, null, null);
		}
		
		switch (shield)
		{
			case SHIELD_DEFENSE_SUCCEED:
			{
				pDef += target.getShldDef();
				break;
			}
			case SHIELD_DEFENSE_PERFECT_BLOCK:
			{
				damage = 1;
				break;
			}
		}
		
		if (target.isAttackable() && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) > 1))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			
			if (crit)
			{
				if (lvlDiff >= Config.NPC_CRIT_DMG_PENALTY.size())
				{
					damage *= Config.NPC_CRIT_DMG_PENALTY.get(Config.NPC_CRIT_DMG_PENALTY.size() - 1);
				}
				else
				{
					damage *= Config.NPC_CRIT_DMG_PENALTY.get(lvlDiff);
				}
			}
			else
			{
				if (lvlDiff >= Config.NPC_DMG_PENALTY.size())
				{
					damage *= Config.NPC_DMG_PENALTY.get(Config.NPC_DMG_PENALTY.size() - 1);
				}
				else
				{
					damage *= Config.NPC_DMG_PENALTY.get(lvlDiff);
				}
			}
		}
		
		return Math.max(damage, 0.5);
	}
	
	public static final double calcEnergyAttackDamage(L2Character attacker, L2Character target, Skill skill, double pAtk, byte shield, boolean crit, boolean ss, double energyChargesBoost, double power)
	{
		double pDef = target.getPDef(attacker);
		L2Weapon weaponItem = attacker.getActiveWeaponItem();
		boolean dual = ((weaponItem != null) && weaponItem.isDual());
		double shotsBonus = ss && dual ? 2.04 : ss ? 2 : 1;
		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		final boolean isPvE = (attacker.isPlayable() && target.isMonster()) || (attacker.isMonster() && target.isPlayable());
		double pvpBonus = 1;
		double pveBonus = 1;
		double penaltyMod = 1;
		
		if (isPvP)
		{
			// Damage bonuses in PvP fight
			pvpBonus = attacker.getStat().calcStat(Stats.PVP_PHYS_SKILL_DMG, 1.0);
			// Defense bonuses in PvP fight
			pDef *= target.getStat().calcStat(Stats.PVP_PHYS_SKILL_DEF, 1.0);
		}
		
		if (isPvE)
		{
			// Damage bonuses in PvP fight
			pveBonus = attacker.getStat().calcStat(Stats.PVE_PHYS_SKILL_DMG, 1.0);
			// Defense bonuses in PvP fight
			pDef *= target.getStat().calcStat(Stats.PVE_PHYS_SKILL_DEF, 1.0);
		}
		
		// Trait, elements
		double weaponTraitMod = calcWeaponTraitBonus(attacker, target);
		double generalTraitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true);
		double attributeMod = calcAttributeBonus(attacker, target, skill);
		
		// Initial damage
		double baseMod = (77.0791 * (power + pAtk)) / pDef;
		
		double damage = baseMod * energyChargesBoost * shotsBonus * pvpBonus * pveBonus * generalTraitMod * weaponTraitMod * attributeMod * penaltyMod;
		
		damage = attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage);
		
		switch (shield)
		{
			case SHIELD_DEFENSE_SUCCEED:
			{
				pDef += target.getShldDef();
				break;
			}
			case SHIELD_DEFENSE_PERFECT_BLOCK:
			{
				damage = 1;
				break;
			}
		}
		
		if ((target instanceof L2Attackable) && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) >= 2))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			
			if (lvlDiff >= Config.NPC_SKILL_DMG_PENALTY.size())
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(Config.NPC_SKILL_DMG_PENALTY.size() - 1);
			}
			else
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(lvlDiff);
			}
			
			if (crit)
			{
				if (lvlDiff >= Config.NPC_CRIT_DMG_PENALTY.size())
				{
					penaltyMod *= Config.NPC_CRIT_DMG_PENALTY.get(Config.NPC_CRIT_DMG_PENALTY.size() - 1);
				}
				else
				{
					penaltyMod *= Config.NPC_CRIT_DMG_PENALTY.get(lvlDiff);
				}
			}
			else
			{
				if (lvlDiff >= Config.NPC_DMG_PENALTY.size())
				{
					penaltyMod *= Config.NPC_DMG_PENALTY.get(Config.NPC_DMG_PENALTY.size() - 1);
				}
				else
				{
					penaltyMod *= Config.NPC_DMG_PENALTY.get(lvlDiff);
				}
			}
		}
		
		if (attacker.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("skillPower", power);
			set.set("baseMod", baseMod);
			set.set("shotsBonus", shotsBonus);
			set.set("energyChargesBoost", energyChargesBoost);
			set.set("pvpBonus", pvpBonus);
			set.set("weaponTraitMod", weaponTraitMod);
			set.set("generalTraitMod", generalTraitMod);
			set.set("attributeMod", attributeMod);
			set.set("pSkillPower", attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage));
			set.set("penaltyMod", penaltyMod);
			set.set("damage", (int) damage);
			Debug.sendSkillDebug(attacker, target, skill, set);
		}
		
		return Math.max(damage, 0.5);
	}
	
	/**
	 * Calculated damage caused by skill ATTACK of attacker on target.
	 * @param attacker player or NPC that makes ATTACK
	 * @param target player or NPC, target of ATTACK
	 * @param skill
	 * @param shld
	 * @param crit if the ATTACK have critical success
	 * @param ss if weapon item was charged by soulshot
	 * @return
	 */
	public static final double calcPAtkDamage(L2Character attacker, L2Character target, Skill skill, double pAtk, byte shield, boolean crit, boolean ss, double proximityBonus, double power)
	{
		double pDef = target.getPDef(attacker);
		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		final boolean isPvE = (attacker.isPlayable() && target.isMonster()) || (attacker.isMonster() && target.isPlayable());
		L2Weapon weaponItem = attacker.getActiveWeaponItem();
		boolean dual = ((weaponItem != null) && weaponItem.isDual());
		double shotsBonus = ss && dual ? 2.04 : ss ? 2 : 1;
		double pvpBonus = 1;
		double pveBonus = 1;
		double penaltyMod = 1;
		
		if (isPvP)
		{
			// Damage bonuses in PvP fight
			pvpBonus = attacker.getStat().calcStat(Stats.PVP_PHYS_SKILL_DMG, 1.0);
			// Defense bonuses in PvP fight
			pDef *= target.getStat().calcStat(Stats.PVP_PHYS_SKILL_DEF, 1.0);
		}
		
		if (isPvE)
		{
			// Damage bonuses in PvP fight
			pveBonus = attacker.getStat().calcStat(Stats.PVE_PHYS_SKILL_DMG, 1.0);
			// Defense bonuses in PvP fight
			pDef *= target.getStat().calcStat(Stats.PVE_PHYS_SKILL_DEF, 1.0);
		}
		
		// Trait, elements
		double weaponTraitMod = calcWeaponTraitBonus(attacker, target);
		double generalTraitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true);
		double attributeMod = calcAttributeBonus(attacker, target, skill);
		double randomMod = attacker.getRandomDamageMultiplier();
		
		// Initial damage
		double baseMod = (77.0791 * (power + pAtk) * proximityBonus * randomMod) / pDef;
		
		double damage = baseMod * shotsBonus * pvpBonus * pveBonus * generalTraitMod * weaponTraitMod * attributeMod * penaltyMod;
		
		damage = attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage);
		
		switch (shield)
		{
			case SHIELD_DEFENSE_SUCCEED:
			{
				pDef += target.getShldDef();
				break;
			}
			case SHIELD_DEFENSE_PERFECT_BLOCK:
			{
				damage = 1;
				break;
			}
		}
		
		if ((target instanceof L2Attackable) && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) >= 2))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			
			if (lvlDiff >= Config.NPC_SKILL_DMG_PENALTY.size())
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(Config.NPC_SKILL_DMG_PENALTY.size() - 1);
			}
			else
			{
				penaltyMod *= Config.NPC_SKILL_DMG_PENALTY.get(lvlDiff);
			}
			
			if (crit)
			{
				if (lvlDiff >= Config.NPC_CRIT_DMG_PENALTY.size())
				{
					penaltyMod *= Config.NPC_CRIT_DMG_PENALTY.get(Config.NPC_CRIT_DMG_PENALTY.size() - 1);
				}
				else
				{
					penaltyMod *= Config.NPC_CRIT_DMG_PENALTY.get(lvlDiff);
				}
			}
			else
			{
				if (lvlDiff >= Config.NPC_DMG_PENALTY.size())
				{
					penaltyMod *= Config.NPC_DMG_PENALTY.get(Config.NPC_DMG_PENALTY.size() - 1);
				}
				else
				{
					penaltyMod *= Config.NPC_DMG_PENALTY.get(lvlDiff);
				}
			}
		}
		
		if (attacker.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("skillPower", power);
			set.set("baseMod", baseMod);
			set.set("shotsBonus", shotsBonus);
			set.set("pvpBonus", pvpBonus);
			set.set("weaponTraitMod", weaponTraitMod);
			set.set("generalTraitMod", generalTraitMod);
			set.set("attributeMod", attributeMod);
			set.set("pSkillPower", attacker.getStat().calcStat(Stats.PHYSICAL_SKILL_POWER, damage));
			set.set("penaltyMod", penaltyMod);
			set.set("damage", (int) damage);
			Debug.sendSkillDebug(attacker, target, skill, set);
		}
		
		return Math.max(damage, 0.5);
	}
	
	public static final double calcMAtkDamage(L2Character attacker, L2Character target, Skill skill, double mAtk, byte shield, boolean sps, boolean bss, boolean mcrit, double power, double shieldDefense)
	{
		double mDef = target.getMDef(attacker, skill);
		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		final boolean isPvE = (attacker.isPlayable() && target.isMonster()) || (attacker.isMonster() && target.isPlayable());
		final double shotsBonus = bss ? 4 : sps ? 2 : 1;
		
		if (isPvP)
		{
			if (skill.isMagic())
			{
				mDef *= target.calcStat(Stats.PVP_MAGICAL_DEF, 1, null, null);
			}
			else
			{
				mDef *= target.calcStat(Stats.PVP_PHYS_SKILL_DEF, 1, null, null);
			}
		}
		
		if (isPvE)
		{
			if (skill.isMagic())
			{
				mDef *= target.calcStat(Stats.PVE_MAGICAL_DEF, 1, null, null);
			}
			else
			{
				mDef *= target.calcStat(Stats.PVE_PHYS_SKILL_DEF, 1, null, null);
			}
		}
		
		// Trait, elements
		final double generalTraitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true);
		final double attributeMod = calcAttributeBonus(attacker, target, skill);
		final double randomMod = attacker.getRandomDamageMultiplier();
		
		double baseMod = (91 * power * Math.sqrt(mAtk * shotsBonus) * randomMod) / mDef;
		
		double damage = baseMod * generalTraitMod * attributeMod;
		
		switch (shield)
		{
			case SHIELD_DEFENSE_SUCCEED:
			{
				mDef += ((target.getShldDef() * (shieldDefense / 100)));
				break;
			}
			case SHIELD_DEFENSE_PERFECT_BLOCK:
			{
				damage = 1;
				break;
			}
		}
		
		// level difference
		double lvlDifference = Math.max((target.getLevel() - attacker.getLevel()) * 2, 1);
		double lvlModifier = 100 - lvlDifference;
		
		if (Config.ALT_GAME_MAGICFAILURES && !calcMagicSuccess(attacker, target, skill))
		{
			if (Rnd.chance(lvlModifier))
			{
				if (attacker.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.DAMAGE_DECREASED_BECAUSE_C1_RESISTED_C2_MAGIC);
					sm.addCharName(target);
					sm.addCharName(attacker);
					attacker.sendPacket(sm);
				}
				
				if (target.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_WEAKLY_RESISTED_C2_MAGIC);
					sm.addCharName(target);
					sm.addCharName(attacker);
					target.sendPacket(sm);
				}
				damage /= 2;
			}
			else
			{
				if (attacker.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_ATTACK_FAILED);
					sm.addCharName(attacker);
					attacker.sendPacket(sm);
				}
				
				if (target.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.RESISTED_C1_MAGIC);
					sm.addCharName(attacker);
					target.sendPacket(sm);
				}
				damage = 1;
			}
		}
		
		if (mcrit)
		{
			damage *= attacker.isPlayable() && target.isPlayable() ? 2.5 : 3;
			damage *= attacker.calcStat(Stats.MAGIC_CRIT_DMG, 1);
			damage += attacker.calcStat(Stats.MAGIC_CRITICAL_DAMAGE_ADD, 0);
		}
		
		if (isPvP)
		{
			Stats stat = skill.isMagic() ? Stats.PVP_MAGICAL_DMG : Stats.PVP_PHYS_SKILL_DMG;
			damage *= attacker.calcStat(stat, 1, null, null);
		}
		
		if (isPvE)
		{
			Stats stat = skill.isMagic() ? Stats.PVE_MAGICAL_DMG : Stats.PVE_PHYS_SKILL_DMG;
			damage *= attacker.calcStat(stat, 1, null, null);
		}
		
		if ((target instanceof L2Attackable) && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) > 1))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			
			if (lvlDiff >= Config.NPC_SKILL_DMG_PENALTY.size())
			{
				damage *= Config.NPC_SKILL_DMG_PENALTY.get(Config.NPC_SKILL_DMG_PENALTY.size() - 1);
			}
			else
			{
				damage *= Config.NPC_SKILL_DMG_PENALTY.get(lvlDiff);
			}
		}
		
		damage = attacker.getStat().calcStat(Stats.MAGICAL_SKILL_POWER, damage);
		
		return Math.max(damage, 0.5);
	}
	
	public static double calcMAtkCubicDamage(CubicInstance attacker, L2Character target, Skill skill, byte shield, boolean mcrit, double power, double shieldDefense)
	{
		double mAtk = attacker.getTemplate().getPower();
		return calcMAtkDamage(attacker.getOwner(), target, skill, mAtk, shield, false, false, mcrit, power, shieldDefense);
	}
	
	public static double calcManaDamage(L2Character attacker, L2Character target, Skill skill, boolean sps, boolean bss, boolean mcrit, double power, double mpBoost)
	{
		double mAtk = attacker.getMAtk(target, skill);
		double mDef = target.getMDef(attacker, skill);
		final double shotbonus = bss ? 4 : sps ? 2 : 1;
		double mp = target.getMaxMp();
		double generalTraitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true);
		
		double baseMod = (Math.sqrt(mAtk * shotbonus) * power * ((mp + mpBoost) / 97)) / mDef;
		
		double damage = baseMod * generalTraitMod;
		
		if (target.isAttackable() && (target.getLevel() >= Config.MIN_NPC_LVL_DMG_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) > 1))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1;
			if (lvlDiff >= Config.NPC_SKILL_DMG_PENALTY.size())
			{
				damage *= Config.NPC_SKILL_DMG_PENALTY.get(Config.NPC_SKILL_DMG_PENALTY.size() - 1);
			}
			else
			{
				damage *= Config.NPC_SKILL_DMG_PENALTY.get(lvlDiff);
			}
		}
		
		// level difference
		double lvlDifference = Math.max((target.getLevel() - attacker.getLevel()) * 2, 1);
		double lvlModifier = 100 - lvlDifference;
		
		if (Config.ALT_GAME_MAGICFAILURES && !calcMagicSuccess(attacker, target, skill))
		{
			if (Rnd.chance(lvlModifier))
			{
				if (attacker.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.DAMAGE_DECREASED_BECAUSE_C1_RESISTED_C2_MAGIC);
					sm.addCharName(target);
					sm.addCharName(attacker);
					attacker.sendPacket(sm);
				}
				
				if (target.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_WEAKLY_RESISTED_C2_MAGIC);
					sm.addCharName(target);
					sm.addCharName(attacker);
					target.sendPacket(sm);
				}
				damage /= 2;
			}
			else
			{
				if (attacker.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_ATTACK_FAILED);
					sm.addCharName(attacker);
					attacker.sendPacket(sm);
				}
				
				if (target.isPlayer())
				{
					SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.RESISTED_C1_MAGIC);
					sm.addCharName(attacker);
					target.sendPacket(sm);
				}
				damage = 1;
			}
		}
		
		if (mcrit)
		{
			damage *= attacker.isPlayable() && target.isPlayable() ? 2.5 : 3;
			damage *= attacker.calcStat(Stats.MAGIC_CRIT_DMG, 1);
			damage += attacker.calcStat(Stats.MAGIC_CRITICAL_DAMAGE_ADD, 0);
			attacker.sendPacket(SystemMessageId.CRITICAL_HIT_MAGIC);
		}
		
		return Math.max(damage, 0.5);
	}
	
	/**
	 * Returns true in case of critical hit
	 * @param attacker
	 * @param target
	 * @return
	 */
	public static final boolean calcCrit(L2Character attacker, L2Character target)
	{
		double criticalMod = attacker.getStat().getCriticalHit(target, null);
		double proximityBonus = attacker.isBehindTarget() ? 1.3 : attacker.isInFrontOfTarget() ? 1 : 1.1;
		double critHeightBonus = ((((Util.constrain(attacker.getZ() - target.getZ(), -25, 25) * 4) / 5) + 10) / 100) + 1;
		double criticalModPos = attacker.getStat().calcStat(Stats.CRITICAL_RATE_POS, 1, target, null);
		double criticalVulnMod = (target.getStat().calcStat(Stats.DEFENCE_CRITICAL_RATE, 1, target, null) + target.getStat().calcStat(Stats.DEFENCE_CRITICAL_RATE_ADD, 0, target, null));
		double rate = proximityBonus * criticalMod * criticalModPos * criticalVulnMod * critHeightBonus;
		
		// critical depends on level difference at high levels.
		if ((attacker.getLevel() >= 78) || (target.getLevel() >= 78))
		{
			rate = rate + ((attacker.getLevel()) * ((attacker.getLevel() - target.getLevel()) * 0.125));
		}
		
		// critical rate is limited between 3%-97%.
		rate = Util.constrain(rate, 30, 970);
		
		return rate > Rnd.get(1000);
	}
	
	/**
	 * Returns true in case of physical skill critical hit
	 * @param attacker
	 * @param target
	 * @param skill
	 * @return
	 */
	public static final boolean calcSkillCrit(L2Character attacker, L2Character target, double rate)
	{
		return ((BaseStats.STR.calcBonus(attacker) * rate) * 10) > (Rnd.get(1000));
	}
	
	/**
	 * Returns true in case of magic skill critical hit
	 * @param mRate
	 * @return
	 */
	public static final boolean calcMCrit(L2Character attacker, L2Character target, double rate)
	{
		// magic critical depends on level difference at high levels.
		if ((attacker.getLevel() >= 78) && (target.getLevel() >= 78))
		{
			rate += (attacker.getLevel()) + ((attacker.getLevel() - target.getLevel()) / 25);
		}
		return rate > Rnd.get(1000);
	}
	
	/**
	 * @param target
	 * @param dmg
	 * @return true in case when ATTACK is canceled due to hit
	 */
	public static final boolean calcAtkBreak(L2Character target, double damage)
	{
		double init = 0;
		
		if (Config.ALT_GAME_CANCEL_CAST && target.isCastingNow())
		{
			init = 15;
		}
		
		if (Config.ALT_GAME_CANCEL_BOW && target.isAttackingNow())
		{
			L2Weapon weaponItem = target.getActiveWeaponItem();
			if ((weaponItem != null) && weaponItem.isRanged())
			{
				init = 15;
			}
		}
		
		if (target.isRaid() || target.isAttackable() || target.isHpBlocked() || (init <= 0))
		{
			return false; // No attack break.
		}
		
		// Chance of break is higher with higher dmg.
		init += Math.sqrt(13 * damage);
		
		// Chance is affected by target MEN.
		init -= ((BaseStats.MEN.calcBonus(target) * 100) - 100);
		
		// Calculate all modifiers for ATTACK_CANCEL.
		double rate = target.calcStat(Stats.P_REDUCE_CANCEL, init, null, null);
		
		// Adjust the rate to be between 1 and 99.
		rate = Math.max(Math.min(rate, 99), 1);
		
		return Rnd.get(100) < rate;
	}
	
	/**
	 * Calculate delay (in milliseconds) for skills cast
	 * @param attacker
	 * @param skill
	 * @param hitTime
	 * @return
	 */
	public static final int calcAtkSpd(L2Character attacker, Skill skill, double hitTime)
	{
		if (skill.isMagic())
		{
			return (int) ((hitTime / attacker.getMAtkSpd()) * 340);
		}
		return (int) ((hitTime / attacker.getPAtkSpd()) * 333);
	}
	
	public static int calculateTimeToHit(int totalAttackTime, WeaponType attackType, boolean twoHanded)
	{
		// Gracia Final Retail confirmed:
		// Time to damage (1 hand, 1 hit): TotalBasicAttackTime * 0.644
		// Time to damage (2 hand, 1 hit): TotalBasicAttackTime * 0.735
		// Time to damage (2 hand, 2 hit): TotalBasicAttackTime * 0.2726 and TotalBasicAttackTime * 0.6
		// Time to damage (bow/xbow): TotalBasicAttackTime * 0.978
		
		// Measured July 2016 by Nik.
		// Due to retail packet delay, we are unable to gather too accurate results. Therefore the below formulas are based on original Gracia Final values.
		// Any original values that appear higher than tested have been replaced with the tested values, because even with packet delay its obvious they are wrong.
		// All other original values are compared with the test results and differences are considered to be too insignificant and mostly caused due to packet delay.
		switch (attackType)
		{
			case BOW:
			case CROSSBOW:
			{
				return (int) (totalAttackTime * 0.95);
			}
			case DUALDAGGER:
			case DUAL:
			case DUALFIST:
			{
				return (int) (totalAttackTime * 0.8726);
			}
			default:
			{
				if (twoHanded)
				{
					return (int) (totalAttackTime * 0.735);
				}
				
				return (int) (totalAttackTime * 0.644);
			}
		}
	}
	
	/**
	 * Formula based on http://l2p.l2wh.com/nonskillattacks.html
	 * @param attacker
	 * @param target
	 * @return {@code true} if hit missed (target evaded), {@code false} otherwise.
	 */
	public static boolean calcHitMiss(L2Character attacker, L2Character target)
	{
		int chance = (80 + (2 * (attacker.getAccuracy() - target.getEvasionRate(attacker)))) * 10;
		
		// Get additional bonus from the conditions when you are attacking
		chance *= HitConditionBonusData.getInstance().getConditionBonus(attacker, target);
		
		chance = Math.max(chance, 200);
		chance = Math.min(chance, 980);
		
		return chance < Rnd.get(1000);
	}
	
	/**
	 * Returns:<br>
	 * 0 = shield defense doesn't succeed<br>
	 * 1 = shield defense succeed<br>
	 * 2 = perfect block<br>
	 * @param attacker
	 * @param target
	 * @param skill
	 * @param sendSysMsg
	 * @return
	 */
	public static byte calcShieldSuccess(L2Character attacker, L2Character target, Skill skill, boolean sendSysMsg)
	{
		final L2Item item = target.getSecondaryWeaponItem();
		if ((item == null) || !(item instanceof L2Armor) || (((L2Armor) item).getItemType() == ArmorType.SIGIL))
		{
			return 0;
		}
		
		double shieldRate = target.calcStat(Stats.SHIELD_RATE, 0, attacker, null) * BaseStats.CON.calcBonus(target);
		if (shieldRate <= 1e-6)
		{
			return 0;
		}
		
		final int degreeside = (int) target.calcStat(Stats.SHIELD_DEFENCE_ANGLE, 0, null, null) + 120;
		if ((degreeside < 360) && (!target.isFacing(attacker, degreeside)))
		{
			return 0;
		}
		
		byte shieldSuccess = SHIELD_DEFENSE_FAILED;
		
		L2Weapon weaponItem = attacker.getActiveWeaponItem();
		if ((weaponItem != null) && weaponItem.isRanged())
		{
			shieldRate *= 3.0;
		}
		
		if (shieldRate > Rnd.get(100))
		{
			if (((100 - (2 * BaseStats.CON.calcBonus(target))) < Rnd.get(100)))
			{
				shieldSuccess = SHIELD_DEFENSE_PERFECT_BLOCK;
			}
			else
			{
				shieldSuccess = SHIELD_DEFENSE_SUCCEED;
			}
		}
		
		if (sendSysMsg && target.isPlayer())
		{
			L2PcInstance enemy = target.getActingPlayer();
			
			switch (shieldSuccess)
			{
				case SHIELD_DEFENSE_SUCCEED:
					enemy.sendPacket(SystemMessageId.SHIELD_DEFENCE_SUCCESSFULL);
					break;
				case SHIELD_DEFENSE_PERFECT_BLOCK:
					enemy.sendPacket(SystemMessageId.YOUR_EXCELLENT_SHIELD_DEFENSE_WAS_A_SUCCESS);
					break;
			}
		}
		return shieldSuccess;
	}
	
	public static byte calcShldUse(L2Character attacker, L2Character target, Skill skill)
	{
		return calcShieldSuccess(attacker, target, skill, true);
	}
	
	public static byte calcShldUse(L2Character attacker, L2Character target)
	{
		return calcShieldSuccess(attacker, target, null, true);
	}
	
	/*
	 * Calculate Probability in following effects:<br> TargetCancel,<br> TargetMeProbability,<br> SkillTurning,<br> Betray,<br> Bluff,<br> DeleteHate,<br> RandomizeHate,<br> DeleteHateOfMe,<br> TransferHate,<br> Confuse<br>
	 * @param chance from effect parameter
	 * @param attacker
	 * @param target
	 * @param skill
	 * @return chance for effect to succeed
	 */
	public static boolean calcProbability(double chance, L2Character attacker, L2Character target, Skill skill)
	{
		if (Double.isNaN(chance) || (chance == -1))
		{
			return calcGeneralTraitBonus(attacker, target, skill.getTraitType(), true) > 0;
		}
		
		int magicLevel = skill.getMagicLevel();
		if (magicLevel <= -1)
		{
			magicLevel = target.getLevel();
		}
		
		final double baseMod = Math.max((((((magicLevel - target.getLevel()) + chance) + 30.0) - target.getINT())), 0);
		final double elementMod = calcAttributeBonus(attacker, target, skill);
		final double traitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), false);
		final double rate = baseMod * elementMod * traitMod;
		
		return Rnd.get(100) < rate;
	}
	
	/**
	 * Calculates the effect landing success.<br>
	 * @param attacker the attacker
	 * @param target the target
	 * @param skill the skill
	 * @return {@code true} if the effect lands
	 */
	public static boolean calcEffectSuccess(L2Character attacker, L2Character target, Skill skill)
	{
		// StaticObjects can not receive continuous effects.
		if (target.isDoor() || (target instanceof L2SiegeFlagInstance) || (target instanceof L2StaticObjectInstance))
		{
			return false;
		}
		
		final int activateRate = skill.getActivateRate();
		if (activateRate == -1)
		{
			return true;
		}
		
		int magicLevel = skill.getMagicLevel();
		if (magicLevel <= -1)
		{
			magicLevel = target.getLevel() + 3;
		}
		
		int basicProperty = 0;
		switch (skill.getBasicProperty())
		{
			case STR:
				basicProperty = target.getSTR();
				break;
			case DEX:
				basicProperty = target.getDEX();
				break;
			case CON:
				basicProperty = target.getCON();
				break;
			case INT:
				basicProperty = target.getINT();
				break;
			case MEN:
				basicProperty = target.getMEN();
				break;
			case WIT:
				basicProperty = target.getWIT();
				break;
		}
		
		final double baseMod = Math.max(((((((magicLevel - target.getLevel()) + 3) * skill.getLvlBonusRate()) + activateRate) + 40.0) - basicProperty), 0);
		final double elementMod = calcAttributeBonus(attacker, target, skill);
		final double traitMod = calcGeneralTraitBonus(attacker, target, skill.getTraitType(), false);
		final double debuffMod = target.calcStat(skill.isDebuff() ? Stats.RESIST_ABNORMAL_DEBUFF : Stats.RESIST_ABNORMAL_BUFF, 1, null, null);
		final double movenentMod = target.calcStat(skill.isDebuff() && (skill.getAbnormalType() == AbnormalType.SPEED_DOWN) ? Stats.RESIST_SPEED_DOWN : Stats.RESIST_ABNORMAL_BUFF, 1, null, null);
		
		final double rate = baseMod * elementMod * traitMod * debuffMod * movenentMod;
		final double successRate = traitMod > 0 ? Util.constrain(rate, 10, 90) : 0;
		
		if (attacker.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("baseMod", baseMod);
			set.set("basicProperty", basicProperty);
			set.set("elementMod", elementMod);
			set.set("traitMod", traitMod);
			set.set("debuffMod", debuffMod);
			set.set("movenentMod", movenentMod);
			set.set("rate", rate);
			set.set("successRate", successRate);
			Debug.sendSkillDebug(attacker, target, skill, set);
		}
		
		if (successRate < Rnd.get(100))
		{
			final SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_RESISTED_YOUR_S2);
			sm.addCharName(target);
			sm.addSkillName(skill);
			attacker.sendPacket(sm);
			return false;
		}
		return true;
	}
	
	public static boolean calcMagicSuccess(L2Character attacker, L2Character target, Skill skill)
	{
		// level difference
		double levelDiff = Math.max((((target.getLevel() - attacker.getLevel())) * 2), 1);
		// penalty modifier
		float targetModifier = 1;
		// chance is affected by target WIT.
		double statBonus = BaseStats.WIT.calcBonus(target);
		// general magic resist
		final double resModifier = target.calcStat(Stats.MAGIC_SUCCESS_RES, 1, null, skill);
		// magic failure rate
		final double failureModifier = attacker.calcStat(Stats.MAGIC_FAILURE_RATE, 1, target, skill);
		
		if (target.isAttackable() && (target.getLevel() >= Config.MIN_NPC_LVL_MAGIC_PENALTY) && (attacker.getActingPlayer() != null) && ((target.getLevel() - attacker.getActingPlayer().getLevel()) >= 3))
		{
			int lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 2;
			if (lvlDiff >= Config.NPC_SKILL_CHANCE_PENALTY.size())
			{
				targetModifier = Config.NPC_SKILL_CHANCE_PENALTY.get(Config.NPC_SKILL_CHANCE_PENALTY.size() - 1);
			}
			else
			{
				targetModifier = Config.NPC_SKILL_CHANCE_PENALTY.get(lvlDiff);
			}
		}
		
		double rate = Math.max((100 - (levelDiff * statBonus * targetModifier * resModifier * failureModifier)), 0);
		
		if (attacker.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("levelDiff", levelDiff);
			set.set("statBonus", statBonus);
			set.set("resModifier", resModifier);
			set.set("failureModifier", failureModifier);
			set.set("targetModifier", targetModifier);
			set.set("rate", rate);
			Debug.sendSkillDebug(attacker, target, skill, set);
		}
		return (Rnd.get(100) < rate);
	}
	
	public static double calculateSkillResurrectRestorePercent(double baseRestorePercent, L2Character caster)
	{
		if ((baseRestorePercent == 0) || (baseRestorePercent == 100))
		{
			return baseRestorePercent;
		}
		
		double restorePercent = baseRestorePercent * BaseStats.WIT.calcBonus(caster);
		if ((restorePercent - baseRestorePercent) > 20.0)
		{
			restorePercent += 20.0;
		}
		
		restorePercent = Math.max(restorePercent, baseRestorePercent);
		restorePercent = Math.min(restorePercent, 90.0);
		
		return restorePercent;
	}
	
	public static boolean calcSkillMastery(L2Character actor, Skill skill)
	{
		// Static Skills are not affected by Skill Mastery.
		if (skill.isStatic() || skill.isReuseDelayLocked() || !actor.isPlayer())
		{
			return false;
		}
		
		final int val = (int) actor.getStat().calcStat(Stats.SKILL_CRITICAL, 0, null, null);
		
		if (val == -1)
		{
			return false;
		}
		
		if (actor.isPlayer())
		{
			double initVal = 0;
			switch (val)
			{
				case 1:
				{
					initVal = (BaseStats.STR).calcBonus(actor);
					break;
				}
				case 4:
				{
					initVal = (BaseStats.INT).calcBonus(actor);
					break;
				}
			}
			initVal *= actor.getStat().calcStat(Stats.SKILL_CRITICAL_PROBABILITY, 1, null, null);
			return (Rnd.get(100) < initVal);
		}
		
		return false;
	}
	
	public static double calcAttributeBonus(L2Character attacker, L2Character target, Skill skill)
	{
		int attack = 0;
		int defense = 0;
		int diff = 0;
		double result = 1.0;
		
		if (skill != null)
		{
			defense = target.getDefenseElementValue(attacker.getAttackElement());
			
			if (attacker.getAttackElement() == skill.getAttributeType().getId())
			{
				attack = skill.getAttributePower() + attacker.getAttackElementValue(attacker.getAttackElement());
			}
			
			diff = attack - defense;
			
			if (diff > 0)
			{
				if (diff < 75)
				{
					result += diff * 0.0052;
				}
				else if (diff < 150)
				{
					result = 1.4;
				}
				else if (diff < 290)
				{
					result = 1.7;
				}
				else if (diff < 300)
				{
					result = 1.8;
				}
				else
				{
					result = 2.0;
				}
			}
			
			if (Config.DEVELOPER || attacker.isDebug())
			{
				_log.info(skill.getName() + ": " + attack + ", " + defense + ", " + result + " Total: " + diff);
			}
		}
		else
		{
			attack = attacker.getAttackElementValue(attacker.getAttackElement());
			defense = target.getDefenseElementValue(attacker.getAttackElement());
			
			diff = attack - defense;
			
			if (diff > 0)
			{
				if (diff < 75)
				{
					result += diff * 0.0052;
				}
				else if (diff < 150)
				{
					result = 1.4;
				}
				else if (diff < 290)
				{
					result = 1.7;
				}
				else if (diff < 300)
				{
					result = 1.8;
				}
				else
				{
					result = 2.0;
				}
			}
			
			if (Config.DEVELOPER || attacker.isDebug())
			{
				_log.info("Hit: " + attack + ", " + defense + ", " + result + " Total: " + diff);
			}
		}
		return result;
	}
	
	public static boolean calcSkillEvasion(L2Character activeChar, L2Character target, Skill skill)
	{
		if (skill.isDebuff())
		{
			return false;
		}
		final double chance = skill.isMagic() ? target.calcStat(Stats.M_SKILL_EVASION, 0, null, skill) : skill.isPhysical() ? target.calcStat(Stats.P_SKILL_EVASION, 0, null, skill) : skill.isStatic() ? target.calcStat(Stats.S_SKILL_EVASION, 0, null, skill) : 0;
		if (Rnd.get(100) < chance)
		{
			if (activeChar.isPlayer())
			{
				SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.C1_DODGES_ATTACK);
				sm.addString(target.getName());
				activeChar.getActingPlayer().sendPacket(sm);
			}
			if (target.isPlayer())
			{
				SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.AVOIDED_C1_ATTACK2);
				sm.addString(activeChar.getName());
				target.getActingPlayer().sendPacket(sm);
			}
			return true;
		}
		
		return false;
	}
	
	/**
	 * Calculate buff/debuff reflection.
	 * @param target
	 * @param skill
	 * @return {@code true} if reflect, {@code false} otherwise.
	 */
	public static boolean calcBuffDebuffReflection(L2Character target, Skill skill)
	{
		if (!skill.isDebuff() || (skill.getActivateRate() == -1))
		{
			return false;
		}
		
		final double chance = skill.isMagic() ? target.calcStat(Stats.REFLECT_SKILL_MAGIC, 0, null, skill) : skill.isPhysical() ? target.calcStat(Stats.REFLECT_SKILL_PHYSIC, 0, null, skill) : skill.isStatic() ? target.calcStat(Stats.REFLECT_SKILL_STATIC, 0, null, skill) : 0;
		
		return chance > Rnd.get(100);
	}
	
	public static boolean calcStunBreak(boolean crit)
	{
		return Rnd.chance(crit ? 30 : 14);
	}
	
	/**
	 * Calculate damage caused by falling
	 * @param cha
	 * @param fallHeight
	 * @return damage
	 */
	public static double calcFallDam(L2Character cha, int fallHeight)
	{
		if (!Config.ENABLE_FALLING_DAMAGE || (fallHeight < 0))
		{
			return 0;
		}
		return cha.calcStat(Stats.FALL, (fallHeight * cha.getMaxHp()) / 1000.0, null, null);
	}
	
	public static boolean calcBlowSuccess(L2Character activeChar, L2Character target, Skill skill, double chanceBoost)
	{
		L2Weapon weaponItem = activeChar.getActiveWeaponItem();
		final double weaponCritical = weaponItem != null ? weaponItem.getStats(Stats.CRITICAL_RATE, activeChar.getTemplate().getBaseCritRate()) : activeChar.getTemplate().getBaseCritRate();
		final double critHeightBonus = ((((Util.constrain(activeChar.getZ() - target.getZ(), -25, 25) * 4) / 5) + 10) / 100) + 1;
		final double criticalModPos = activeChar.getStat().calcStat(Stats.CRITICAL_RATE_POS, 1, target, null);
		final double proximityBonus = (activeChar.isInFrontOfTarget()) ? 1 : (activeChar.isBehindTarget()) ? 1.3 : 1.1;
		final double chanceBoostMod = (100 + chanceBoost) / 100;
		final double blowRateMod = activeChar.calcStat(Stats.BLOW_RATE, 1);
		final double rate = weaponCritical * chanceBoostMod * proximityBonus * criticalModPos * critHeightBonus * blowRateMod;
		final double successRate = Math.min(rate, 80);
		
		if (activeChar.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("weaponCritical", weaponCritical);
			set.set("critHeightBonus", critHeightBonus);
			set.set("proximityBonus", proximityBonus);
			set.set("criticalModPos", criticalModPos);
			set.set("blowRateMod", blowRateMod);
			set.set("chanceBoostMod", chanceBoostMod);
			set.set("rate: (max 80 of 100)", successRate);
			Debug.sendSkillDebug(activeChar, target, skill, set);
		}
		return Rnd.get(1000) < (successRate * 10);
	}
	
	public static List<BuffInfo> calcCancelEffects(L2Character activeChar, L2Character target, Skill skill, DispelCategory slot, int rate, int max)
	{
		final List<BuffInfo> canceled = new ArrayList<>(max);
		switch (slot)
		{
			case buff:
			{
				// Resist Modifier.
				final int magicLevel = skill.getMagicLevel();
				
				// Prevent initialization.
				final List<BuffInfo> buffs = target.getEffectList().hasBuffs() ? new ArrayList<>(target.getEffectList().getBuffs()) : new ArrayList<>(1);
				if (target.getEffectList().hasDances())
				{
					buffs.addAll(target.getEffectList().getDances());
				}
				if (target.getEffectList().hasTriggered())
				{
					buffs.addAll(target.getEffectList().getTriggered());
				}
				for (int i = buffs.size() - 1; i >= 0; i--) // reverse order
				{
					BuffInfo info = buffs.get(i);
					if (!info.getSkill().canBeStolen() || ((rate < 100) && (!calcCancelSuccess(info, magicLevel, rate, skill, activeChar, target))))
					{
						continue;
					}
					canceled.add(info);
					if (canceled.size() >= max)
					{
						break;
					}
				}
				break;
			}
			case debuff:
			{
				final List<BuffInfo> debuffs = new ArrayList<>(target.getEffectList().getDebuffs());
				for (int i = debuffs.size() - 1; i >= 0; i--)
				{
					BuffInfo info = debuffs.get(i);
					if (info.getSkill().isDebuff() && !info.getSkill().isIrreplaceableBuff() && (Rnd.get(100) <= rate))
					{
						canceled.add(info);
						if (canceled.size() >= max)
						{
							break;
						}
					}
				}
				break;
			}
		}
		return canceled;
	}
	
	public static List<BuffInfo> calcStealEffects(L2Character activeChar, L2Character target, Skill skill, DispelCategory slot, int rate, int max)
	{
		final List<BuffInfo> canceled = new ArrayList<>(max);
		switch (slot)
		{
			case buff:
			{
				// Resist Modifier.
				final int magicLevel = skill.getMagicLevel();
				
				// Prevent initialization.
				final List<BuffInfo> buffs = target.getEffectList().hasBuffs() ? new ArrayList<>(target.getEffectList().getBuffs()) : new ArrayList<>(max);
				if (target.getEffectList().hasDances())
				{
					buffs.addAll(target.getEffectList().getDances());
				}
				if (target.getEffectList().hasTriggered())
				{
					buffs.addAll(target.getEffectList().getTriggered());
				}
				
				int pos = max;
				for (int i = buffs.size() - 1; i >= 0; i--)
				{
					BuffInfo info = buffs.get(i);
					if (!info.getSkill().canBeStolen())
					{
						continue;
					}
					pos--;
					if (calcCancelSuccess(info, magicLevel, rate, skill, activeChar, target))
					{
						canceled.add(info);
					}
					if (pos < 1)
					{
						break;
					}
				}
				break;
			}
			case debuff:
			{
				final List<BuffInfo> debuffs = new ArrayList<>(target.getEffectList().getDebuffs());
				for (int i = debuffs.size() - 1; i >= 0; i--)
				{
					BuffInfo info = debuffs.get(i);
					if (info.getSkill().isDebuff() && !info.getSkill().isIrreplaceableBuff() && (Rnd.get(100) <= rate))
					{
						canceled.add(info);
						if (canceled.size() >= max)
						{
							break;
						}
					}
				}
				break;
			}
		}
		return canceled;
	}
	
	public static boolean calcCancelSuccess(BuffInfo info, int magicLevel, int rate, Skill skill, L2Character activeChar, L2Character target)
	{
		final double baseMod = (rate + ((((magicLevel - info.getSkill().getMagicLevel()) * 2))));
		final double resistMod = target.calcStat(Stats.RESIST_DISPEL_BUFF, 1, null, null);
		final double timeBuffMod = (info.getAbnormalTime() / 120);
		final double result = Util.constrain((baseMod + timeBuffMod) * resistMod, 25, 75);
		
		if (activeChar.isDebug())
		{
			final StatsSet set = new StatsSet();
			set.set("baseMod:", baseMod);
			set.set("resistMod:", resistMod);
			set.set("timeBuffMod:", timeBuffMod);
			set.set("rate: (min 25, max 75): ", result);
			Debug.sendSkillDebug(activeChar, target, skill, set);
		}
		
		return Rnd.get(100) < result;
	}
	
	/**
	 * Calculates the abnormal time for an effect.<br>
	 * The abnormal time is taken from the skill definition, and it's global for all effects present in the skills.
	 * @param caster the caster
	 * @param target the target
	 * @param skill the skill
	 * @return the time that the effect will last
	 */
	public static int calcEffectAbnormalTime(L2Character attacker, L2Character target, Skill skill)
	{
		int abnormalTime = (skill == null) || skill.isPassive() || skill.isToggle() ? -1 : skill.getAbnormalTime();
		
		// If the skill is a mastery skill, the effect will last twice the default time.
		if ((skill != null) && Formulas.calcSkillMastery(attacker, skill))
		{
			abnormalTime *= 2;
		}
		
		return abnormalTime;
	}
	
	/**
	 * Calculates karma lost upon death.
	 * @param player
	 * @param exp
	 * @return the amount of karma player has loosed.
	 */
	public static int calculateKarmaLost(L2PcInstance player, long exp)
	{
		double karmaLooseMul = KarmaData.getInstance().getMultiplier(player.getLevel());
		if (exp > 0) // Received exp
		{
			exp /= Config.RATE_KARMA_LOST;
		}
		return (int) ((Math.abs(exp) / karmaLooseMul) / 30);
	}
	
	/**
	 * Calculates karma gain upon playable kill.</br>
	 * Updated to High Five on 10.09.2014 by Zealar tested in retail.
	 * @param pkCount
	 * @param isSummon
	 * @return karma points that will be added to the player.
	 */
	public static int calculateKarmaGain(int pkCount, boolean isSummon)
	{
		int result = 43200;
		
		if (isSummon)
		{
			result = (int) ((((pkCount * 0.375) + 1) * 60) * 4) - 150;
			
			if (result > 10800)
			{
				return 10800;
			}
		}
		
		if (pkCount < 99)
		{
			result = (int) ((((pkCount * 0.5) + 1) * 60) * 12);
		}
		else if (pkCount < 180)
		{
			result = (int) ((((pkCount * 0.125) + 37.75) * 60) * 12);
		}
		
		return result;
	}
	
	public static double calcGeneralTraitBonus(L2Character attacker, L2Character target, TraitType traitType, boolean ignoreResistance)
	{
		if (traitType == TraitType.NONE)
		{
			return 1.0;
		}
		
		if (target.getStat().isTraitInvul(traitType))
		{
			return 0;
		}
		
		switch (traitType.getType())
		{
			case 2:
			{
				if (!attacker.getStat().hasAttackTrait(traitType) || !target.getStat().hasDefenceTrait(traitType))
				{
					return 1.0;
				}
				break;
			}
			case 3:
			{
				if (ignoreResistance)
				{
					return 1.0;
				}
				break;
			}
			default:
			{
				return 1.0;
			}
		}
		
		final double result = (attacker.getStat().getAttackTrait(traitType) - target.getStat().getDefenceTrait(traitType)) + 1.0;
		return Util.constrain(result, 0.05, 2.0);
	}
	
	public static double calcWeaponTraitBonus(L2Character attacker, L2Character target)
	{
		double result = target.getStat().getDefenceTrait(attacker.getAttackType().getTraitType()) - 1.0;
		return 1.0 - result;
	}
	
	public static double calcAttackTraitBonus(L2Character attacker, L2Character target)
	{
		final double weaponTraitBonus = calcWeaponTraitBonus(attacker, target);
		if (weaponTraitBonus == 0)
		{
			return 0;
		}
		
		double weaknessBonus = 1.0;
		for (TraitType traitType : TraitType.values())
		{
			if (traitType.getType() == 2)
			{
				weaknessBonus *= calcGeneralTraitBonus(attacker, target, traitType, true);
				if (weaknessBonus == 0)
				{
					return 0;
				}
			}
		}
		
		return Util.constrain((weaponTraitBonus * weaknessBonus), 0.05, 2.0);
	}
}