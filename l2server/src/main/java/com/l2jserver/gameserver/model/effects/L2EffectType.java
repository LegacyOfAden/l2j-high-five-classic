/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.effects;

/**
 * Effect types.
 * @author nBd
 */
public enum L2EffectType
{
	AGGRESSION,
	BLOCK_CONTROL,
	BLOCK_SKILL_PHYSICAL,
	BLOCK_SPELL,
	BLOCK_MOVE,
	BLOCK_ACT,
	BLINK,
	BLUFF,
	BUFF,
	CHAT_BLOCK,
	CP_DAM_PERCENT,
	CP,
	CONSUME_BODY,
	DEBUFF,
	DISPEL,
	DISPEL_BY_SLOT,
	DMG_OVER_TIME,
	DEATH_LINK,
	FAKE_DEATH,
	FEAR,
	FISHING,
	FISHING_START,
	HATE,
	HP,
	HIDE,
	HP_DRAIN,
	LETHAL_ATTACK,
	MAGICAL_ATTACK,
	MANAHEAL_BY_LEVEL,
	MANAHEAL_PERCENT,
	MUTE,
	NEVIT_HOURGLASS,
	NOBLESSE_BLESSING,
	NONE,
	PASSIVE,
	PHYSICAL_ATTACK,
	REBALANCE_HP,
	REFUEL_AIRSHIP,
	RELAXING,
	RESURRECTION,
	RESURRECTION_SPECIAL,
	SKILL_TURNING,
	SPOIL,
	STEAL_ABNORMAL,
	SUMMON,
	SUMMON_NPC,
	TARGET_CANCEL,
	TELEPORT,
	TELEPORT_TO_TARGET
}