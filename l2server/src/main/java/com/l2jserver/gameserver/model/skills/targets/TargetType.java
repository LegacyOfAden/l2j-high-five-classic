/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.skills.targets;

/**
 * Target type enumerated.
 * @author Zoey76
 */
public enum TargetType
{
	/** Advance Head Quarters (Outposts). */
	advance_base,
	/** Enemies in high terrain or protected by castle walls and doors. */
	artillery,
	/** Doors or treasure chests. */
	door_treasure,
	/** Any enemies (included allies). */
	enemy,
	/** Friendly. */
	enemy_not,
	/** Only enemies (not included allies). */
	enemy_only,
	/** Fortress's Flagpole. */
	fortress_flagpole,
	/** Ground. */
	ground,
	/** Holy Artifacts from sieges. */
	holything,
	/** Items. */
	item,
	/** Me or my party (if any). Seen in aura skills. */
	my_party,
	/** Nothing. */
	none,
	/** NPC corpses. */
	npc_body,
	/** Others, except caster. */
	others,
	/** Pet's owner. */
	owner_pet,
	/** Player corpses. */
	pc_body,
	/** Self. */
	self,
	/** Servitor or pet. */
	summon,
	/** Anything targetable. */
	target,
	/** Wyverns. */
	wyvern_target,
}
