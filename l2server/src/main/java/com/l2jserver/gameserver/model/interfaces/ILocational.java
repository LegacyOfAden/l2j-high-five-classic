/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.interfaces;

/**
 * Object world location storage interface.
 * @author xban1x
 */
public interface ILocational
{
	/**
	 * Gets the X coordinate of this object.
	 * @return the X coordinate
	 */
	public int getX();
	
	/**
	 * Gets the Y coordinate of this object.
	 * @return the current Y coordinate
	 */
	public int getY();
	
	/**
	 * Gets the Z coordinate of this object.
	 * @return the current Z coordinate
	 */
	public int getZ();
	
	/**
	 * Gets the heading of this object.
	 * @return the current heading
	 */
	public int getHeading();
	
	/**
	 * Gets the instance zone ID of this object.
	 * @return the ID of the instance zone this object is currently in (0 - not in any instance)
	 */
	public int getInstanceId();
	
	/**
	 * Computes the 2D Euclidean distance between this locational and locational loc.
	 * @param loc the locational
	 * @return the 2D Euclidean distance between this locational and locational loc
	 */
	default double distance2d(ILocational loc)
	{
		return distance2d(loc.getX(), loc.getY());
	}
	
	/**
	 * Computes the 2D Euclidean distance between this locational and (x, y).
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @return the 2D Euclidean distance between this locational and (x, y)
	 */
	default double distance2d(double x, double y)
	{
		return Math.sqrt(Math.pow(getX() - x, 2) + Math.pow(getY() - y, 2));
	}
	
	/**
	 * Checks if this locational is in 2D Euclidean radius of (x, y)
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of (x, y), {@code false} otherwise
	 */
	default boolean isInRadius2d(double x, double y, double radius)
	{
		return distance2d(x, y) <= radius;
	}
	
	/**
	 * Checks if this locational is in 3D Euclidean radius of (x, y, z)
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of (x, y, z), {@code false} otherwise
	 */
	default boolean isInRadius3d(double x, double y, double z, double radius)
	{
		return distance3d(x, y, z) <= radius;
	}
	
	/**
	 * Checks if this locational is in 3D Euclidean radius of locational loc
	 * @param loc the locational
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of locational loc, {@code false} otherwise
	 */
	default boolean isInRadius3d(ILocational loc, double radius)
	{
		return isInRadius3d(loc.getX(), loc.getY(), loc.getZ(), radius);
	}
	
	/**
	 * Checks if this locational is in 2D Euclidean radius of locational loc
	 * @param loc the locational
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of locational loc, {@code false} otherwise
	 */
	default boolean isInRadius2d(ILocational loc, double radius)
	{
		return isInRadius2d(loc.getX(), loc.getY(), radius);
	}
	
	/**
	 * Computes the 3D Euclidean distance between this locational and (x, y, z).
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 * @return the 3D Euclidean distance between this locational and (x, y, z)
	 */
	default double distance3d(double x, double y, double z)
	{
		return Math.sqrt(Math.pow(getX() - x, 2) + Math.pow(getY() - y, 2) + Math.pow(getZ() - z, 2));
	}
	
	/**
	 * Computes the 3D Euclidean distance between this locational and locational loc.
	 * @param loc the locational
	 * @return the 3D Euclidean distance between this locational and locational loc
	 */
	default double distance3d(ILocational loc)
	{
		return distance3d(loc.getX(), loc.getY(), loc.getZ());
	}
	
	/**
	 * Gets this object's location.
	 * @return a {@link ILocational} object containing the current position of this object
	 */
	public ILocational getLocation();
}
