package com.l2jserver.gameserver.ai.model;

import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_ACTIVE;

import java.util.Collection;

import com.l2jserver.gameserver.ai.L2FortSiegeGuardAI;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2DefenderInstance;
import com.l2jserver.gameserver.model.effects.L2EffectType;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * This vGodFather
 */
public class SiegeHealerAI extends L2FortSiegeGuardAI
{
	public SiegeHealerAI(L2DefenderInstance creature)
	{
		super(creature);
	}
	
	@Override
	protected void attackPrepare()
	{
		final L2Character mostHate = ((L2Attackable) _actor).getMostHated();
		if (mostHate == null)
		{
			setIntention(AI_INTENTION_ACTIVE);
			return;
		}
		
		setAttackTarget(mostHate);
		
		final L2Character npc = getActor();
		
		Collection<Skill> skills = _actor.getAllSkills();
		if (!_actor.isBlockSpell() && !skills.isEmpty())
		{
			for (L2Character obj : npc.getKnownList().getKnownCharactersInRadius(300))
			{
				if (!(obj instanceof L2DefenderInstance))
				{
					continue;
				}
				
				for (Skill sk : skills)
				{
					if (sk.isPassive())
					{
						continue;
					}
					
					if (!checkSkillCastConditions(npc.getAttackable(), sk))
					{
						continue;
					}
					
					boolean docast = false;
					if ((sk.hasEffectType(L2EffectType.HP) && (obj.getCurrentHp() < (obj.getMaxHp() * 0.9))))
					{
						docast = true;
					}
					
					if ((sk.isGood() && !_actor.isAffectedBySkill(sk.getId())))
					{
						docast = true;
					}
					
					if (docast)
					{
						if (maybeMoveToPawn(obj, _actor.getMagicalAttackRange(sk)))
						{
							return;
						}
						
						clientStopMoving(null);
						npc.setTarget(obj);
						npc.doCast(sk);
						
						if (npc.isDebug())
						{
							LOG.info("{} used skill {} on target: " + npc.getTarget(), this, sk);
						}
						return;
					}
				}
			}
		}
		
		if (_actor.isBlockSpell())
		{
			super.attackPrepare();
		}
	}
}