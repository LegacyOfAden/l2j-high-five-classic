/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.ai;

import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_ATTACK;
import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_CAST;
import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_FOLLOW;
import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_IDLE;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.AutoAttackStart;
import com.l2jserver.gameserver.network.serverpackets.AutoAttackStop;
import com.l2jserver.gameserver.network.serverpackets.Die;
import com.l2jserver.gameserver.network.serverpackets.MoveToLocation;
import com.l2jserver.gameserver.network.serverpackets.MoveToPawn;
import com.l2jserver.gameserver.network.serverpackets.StopMove;
import com.l2jserver.gameserver.network.serverpackets.StopRotation;
import com.l2jserver.gameserver.network.serverpackets.ValidateLocation;
import com.l2jserver.gameserver.taskmanager.AttackStanceTaskManager;
import com.l2jserver.util.Rnd;

/**
 * Mother class of all objects AI in the world.<br>
 * AbastractAI :<br>
 * <li>L2CharacterAI</li>
 */
public abstract class AbstractAI implements Ctrl
{
	protected static final Logger LOG = LoggerFactory.getLogger(AbstractAI.class);
	
	private NextAction _nextAction;
	
	/**
	 * @return the _nextAction
	 */
	public NextAction getNextAction()
	{
		return _nextAction;
	}
	
	/**
	 * @param nextAction the next action to set.
	 */
	public void setNextAction(NextAction nextAction)
	{
		_nextAction = nextAction;
	}
	
	/**
	 * Create and Launch an AI Follow Task to execute every 0.5s, following at specified range.
	 * @param target The L2Character to follow
	 * @param range
	 */
	public synchronized void startFollow(L2Object target, double range)
	{
		if (_followTask != null)
		{
			_followTask.cancel(false);
			_followTask = null;
		}
		
		_followTarget = target;
		_followTask = ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new AttackTask(range), 5, 1000);
	}
	
	private class AttackTask implements Runnable
	{
		protected double _range;
		
		public AttackTask(double range)
		{
			_range = range;
		}
		
		@Override
		public void run()
		{
			try
			{
				if (_followTask == null)
				{
					return;
				}
				
				if (_actor.isAttackingNow() || _actor.isCastingNow() || _actor.isBlockControl() || (_actor.getQueuedSkill() != null))
				{
					return;
				}
				
				final L2Object followTarget = _followTarget;
				
				if (followTarget == null)
				{
					return;
				}
				
				// Get current position of the player
				final int curX = _actor.getX();
				final int curY = _actor.getY();
				
				// Get current position of the target
				int px = followTarget.getX();
				int py = followTarget.getY();
				int pz = followTarget.getZ();
				
				final double angle_radius = Math.min(_range, 100);
				final double angle = Math.toRadians(Rnd.get(-90, 90)) + Math.atan2(py - curY, px - curX);
				final int x = px + (int) (angle_radius * Math.cos(angle));
				final int y = py + (int) (angle_radius * Math.sin(angle));
				
				if ((_actor.getAI().getIntention() == AI_INTENTION_CAST) && (_range > 100))
				{
					if (((L2Character) followTarget).isMoving())
					{
						if (!_actor.isInsideRadius(px, py, pz, _range + 40, false, false))
						{
							moveTo(px, py, pz, (int) _range, true, false);
						}
					}
					else if (!_actor.isInsideRadius(px, py, pz, _range, false, false))
					{
						moveTo(px, py, pz, (int) _range, true, false);
					}
				}
				else
				{
					if (((L2Character) followTarget).isMoving())
					{
						if (!_actor.isInsideRadius(px, py, pz, _range + 40, false, false))
						{
							moveTo(x, y, pz, (int) _range, false, false);
						}
					}
					else if (!_actor.isInsideRadius(px, py, pz, _range, false, false))
					{
						moveTo(x, y, pz, (int) _range, false, false);
					}
				}
			}
			catch (Exception e)
			{
				LOG.warn(getClass().getSimpleName() + ": Error: " + e.getMessage());
			}
		}
	}
	
	private class FollowTask implements Runnable
	{
		
		public FollowTask()
		{
		}
		
		@Override
		public void run()
		{
			try
			{
				if (_followTask == null)
				{
					return;
				}
				
				if (_actor.isAttackingNow() || _actor.isCastingNow() || _actor.isBlockControl() || (_actor.getQueuedSkill() != null))
				{
					return;
				}
				
				final L2Object target = _followTarget;
				int offset = 36;
				int offsetWithCollision = offset + _actor.getTemplate().getCollisionRadius();
				if (target.isCharacter())
				{
					offsetWithCollision += ((L2Character) target).getTemplate().getCollisionRadius();
				}
				
				int x = target.getX();
				int y = target.getY();
				int z = target.getZ();
				
				if (_actor.getAI().getIntention() == AI_INTENTION_FOLLOW)
				{
					if (!_actor.isPlayer())
					{
						if (!_actor.isInsideRadius2D(x, y, z, offsetWithCollision + offsetWithCollision))
						{
							moveTo(x, y, z, offsetWithCollision, true, false);
						}
					}
					else
					{
						moveToPawn(target, 100, true, false);
					}
				}
			}
			catch (Exception e)
			{
				LOG.warn(getClass().getSimpleName() + ": Error: " + e.getMessage());
			}
		}
	}
	
	/** The character that this AI manages */
	protected final L2Character _actor;
	
	/** Current long-term intention */
	protected CtrlIntention _intention = AI_INTENTION_IDLE;
	/** Current long-term intention parameter */
	protected Object[] _intentionArgs = null;
	/** Current long-term intention parameter */
	protected Object _intentionArg0 = null;
	/** Current long-term intention parameter */
	protected Object _intentionArg1 = null;
	
	/** Flags about client's state, in order to know which messages to send */
	protected volatile boolean _clientMoving;
	/** Flags about client's state, in order to know which messages to send */
	protected volatile boolean _clientAutoAttacking;
	/** Flags about client's state, in order to know which messages to send */
	protected double _clientMovingToPawnOffset;
	
	/** Different targets this AI maintains */
	private L2Object _target;
	private L2Character _castTarget;
	protected L2Character _attackTarget;
	protected L2Object _followTarget;
	
	/** The skill we are currently casting by INTENTION_CAST */
	Skill _skill;
	
	/** Different internal state flags */
	private int _moveToPawnTimeout;
	
	protected Future<?> _followTask = null;
	
	/**
	 * Constructor of AbstractAI.
	 * @param creature the creature
	 */
	protected AbstractAI(L2Character creature)
	{
		_actor = creature;
	}
	
	/**
	 * @return the L2Character managed by this Accessor AI.
	 */
	@Override
	public L2Character getActor()
	{
		return _actor;
	}
	
	/**
	 * @return the current Intention.
	 */
	@Override
	public CtrlIntention getIntention()
	{
		return _intention;
	}
	
	protected void setCastTarget(L2Character target)
	{
		_castTarget = target;
	}
	
	/**
	 * @return the current cast target.
	 */
	public L2Character getCastTarget()
	{
		return _castTarget;
	}
	
	protected void setAttackTarget(L2Character target)
	{
		_attackTarget = target;
	}
	
	/**
	 * @return current attack target.
	 */
	@Override
	public L2Character getAttackTarget()
	{
		return _attackTarget;
	}
	
	/**
	 * Set the Intention of this AbstractAI.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method is USED by AI classes</B></FONT><B><U><br>
	 * Overridden in </U> : </B><BR>
	 * <B>L2AttackableAI</B> : Create an AI Task executed every 500s (if necessary)<BR>
	 * <B>L2SummonAI</B> : Create an AI Task executed every 500s (if necessary)<BR>
	 * <B>L2PlayerAI</B> : Stores the current AI intention parameters to later restore it if necessary.
	 * @param intention The new Intention to set to the AI
	 * @param arg0 The first parameter of the Intention
	 * @param arg1 The second parameter of the Intention
	 */
	synchronized void changeIntention(CtrlIntention intention, Object arg0, Object arg1)
	{
		_intention = intention;
		_intentionArg0 = arg0;
		_intentionArg1 = arg1;
	}
	
	/**
	 * Launch the L2CharacterAI onIntention method corresponding to the new Intention.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Stop the FOLLOW mode if necessary</B></FONT>
	 * @param intention The new Intention to set to the AI
	 */
	@Override
	public final void setIntention(CtrlIntention intention)
	{
		setIntention(intention, null, null);
	}
	
	/**
	 * Launch the L2CharacterAI onIntention method corresponding to the new Intention.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Stop the FOLLOW mode if necessary</B></FONT>
	 * @param intention The new Intention to set to the AI
	 * @param arg0 The first parameter of the Intention (optional target)
	 */
	@Override
	public final void setIntention(CtrlIntention intention, Object arg0)
	{
		setIntention(intention, arg0, null);
	}
	
	@Override
	public final void setIntention(CtrlIntention intention, Object arg0, Object arg1)
	{
		// Stop the follow mode if necessary
		if ((intention != AI_INTENTION_FOLLOW) && (intention != AI_INTENTION_ATTACK))
		{
			stopFollow();
		}
		
		// Launch the onIntention method of the L2CharacterAI corresponding to the new Intention
		switch (intention)
		{
			case AI_INTENTION_IDLE:
				onIntentionIdle();
				break;
			case AI_INTENTION_ACTIVE:
				onIntentionActive();
				break;
			case AI_INTENTION_REST:
				onIntentionRest();
				break;
			case AI_INTENTION_ATTACK:
				onIntentionAttack((L2Character) arg0);
				break;
			case AI_INTENTION_CAST:
				onIntentionCast((Skill) arg0, (L2Object) arg1);
				break;
			case AI_INTENTION_MOVE_TO:
				onIntentionMoveTo((Location) arg0);
				break;
			case AI_INTENTION_FOLLOW:
				onIntentionFollow((L2Character) arg0);
				break;
			case AI_INTENTION_PICK_UP:
				onIntentionPickUp((L2Object) arg0);
				break;
			case AI_INTENTION_INTERACT:
				onIntentionInteract((L2Object) arg0);
				break;
		}
		
		// If do move or follow intention drop next action.
		if ((_nextAction != null) && _nextAction.getIntentions().contains(intention))
		{
			_nextAction = null;
		}
	}
	
	/**
	 * Launch the L2CharacterAI onEvt method corresponding to the Event.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned period)</B></FONT>
	 * @param evt The event whose the AI must be notified
	 */
	@Override
	public final void notifyEvent(CtrlEvent evt)
	{
		notifyEvent(evt, null, null);
	}
	
	/**
	 * Launch the L2CharacterAI onEvt method corresponding to the Event. <FONT COLOR=#FF0000><B> <U>Caution</U> : The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned period)</B></FONT>
	 * @param evt The event whose the AI must be notified
	 * @param arg0 The first parameter of the Event (optional target)
	 */
	@Override
	public final void notifyEvent(CtrlEvent evt, Object arg0)
	{
		notifyEvent(evt, arg0, null);
	}
	
	/**
	 * Launch the L2CharacterAI onEvt method corresponding to the Event. <FONT COLOR=#FF0000><B> <U>Caution</U> : The current general intention won't be change (ex : If the character attack and is stunned, he will attack again after the stunned period)</B></FONT>
	 * @param evt The event whose the AI must be notified
	 */
	@Override
	public final void notifyEvent(CtrlEvent evt, Object... args)
	{
		if ((!_actor.isVisible() && !_actor.isTeleporting()) || !_actor.hasAI())
		{
			return;
		}
		
		switch (evt)
		{
			case EVT_THINK:
				onEvtThink();
				break;
			case EVT_ATTACKED:
				onEvtAttacked((L2Character) args[0]);
				break;
			case EVT_AGGRESSION:
				onEvtAggression((L2Character) args[0], ((Number) args[1]).intValue());
				break;
			case EVT_BLOCK_ACTION:
				onEvtBlockAction((L2Character) args[0]);
				break;
			case EVT_BLOCK_MOVE:
				onEvtBlockMove((L2Character) args[0]);
				break;
			case EVT_CONFUSED:
				onEvtConfused((L2Character) args[0]);
				break;
			case EVT_MUTED:
				onEvtMuted((L2Character) args[0]);
				break;
			case EVT_EVADED:
				onEvtEvaded((L2Character) args[0]);
				break;
			case EVT_READY_TO_ACT:
				if (!_actor.isCastingNow())
				{
					onEvtReadyToAct();
				}
				break;
			case EVT_USER_CMD:
				onEvtUserCmd(args[0], args[1]);
				break;
			case EVT_ARRIVED:
				// happens e.g. from stopmove but we don't process it if we're casting
				if (!_actor.isCastingNow() && !_actor.isCastingSimultaneouslyNow())
				{
					onEvtArrived();
				}
				break;
			case EVT_ARRIVED_REVALIDATE:
				// this is disregarded if the char is not moving any more
				if (_actor.isMoving())
				{
					onEvtArrivedRevalidate();
				}
				break;
			case EVT_ARRIVED_BLOCKED:
				onEvtArrivedBlocked((Location) args[0]);
				break;
			case EVT_FORGET_OBJECT:
				onEvtForgetObject((L2Object) args[0]);
				break;
			case EVT_CANCEL:
				onEvtCancel();
				break;
			case EVT_DEAD:
				onEvtDead();
				break;
			case EVT_FAKE_DEATH:
				onEvtFakeDeath();
				break;
			case EVT_FINISH_CASTING:
				onEvtFinishCasting();
				break;
			case EVT_AFRAID:
			{
				onEvtAfraid((L2Character) args[0], (Boolean) args[1]);
				break;
			}
		}
		
		// Do next action.
		if ((_nextAction != null) && _nextAction.getEvents().contains(evt))
		{
			_nextAction.doAction();
		}
	}
	
	protected abstract void onIntentionIdle();
	
	protected abstract void onIntentionActive();
	
	protected abstract void onIntentionRest();
	
	protected abstract void onIntentionAttack(L2Character target);
	
	protected abstract void onIntentionCast(Skill skill, L2Object target);
	
	protected abstract void onIntentionMoveTo(Location destination);
	
	protected abstract void onIntentionFollow(L2Character target);
	
	protected abstract void onIntentionPickUp(L2Object item);
	
	protected abstract void onIntentionInteract(L2Object object);
	
	protected abstract void onEvtThink();
	
	protected abstract void onEvtAttacked(L2Character attacker);
	
	protected abstract void onEvtAggression(L2Character target, long aggro);
	
	protected abstract void onEvtBlockAction(L2Character attacker);
	
	protected abstract void onEvtBlockMove(L2Character attacker);
	
	protected abstract void onEvtConfused(L2Character attacker);
	
	protected abstract void onEvtMuted(L2Character attacker);
	
	protected abstract void onEvtEvaded(L2Character attacker);
	
	protected abstract void onEvtReadyToAct();
	
	protected abstract void onEvtUserCmd(Object arg0, Object arg1);
	
	protected abstract void onEvtArrived();
	
	protected abstract void onEvtArrivedRevalidate();
	
	protected abstract void onEvtArrivedBlocked(Location blocked_at_pos);
	
	protected abstract void onEvtForgetObject(L2Object object);
	
	protected abstract void onEvtCancel();
	
	protected abstract void onEvtDead();
	
	protected abstract void onEvtFakeDeath();
	
	protected abstract void onEvtFinishCasting();
	
	protected abstract void onEvtAfraid(L2Character effector, boolean start);
	
	/**
	 * Cancel action client side by sending Server->Client packet ActionFailed to the L2PcInstance actor. <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 */
	protected void clientActionFailed()
	{
		if (_actor instanceof L2PcInstance)
		{
			_actor.sendPacket(ActionFailed.STATIC_PACKET);
		}
	}
	
	protected void moveToPawn(L2Object pawn, double offset, boolean calculateDistance, boolean calculateCollisionRadius)
	{
		if (_actor.isMovementDisabled() || (_actor.getMoveSpeed() <= 0))
		{
			return;
		}
		
		if (_clientMoving && (_target == pawn))
		{
			if (_clientMovingToPawnOffset == offset)
			{
				if (GameTimeController.getInstance().getGameTicks() < _moveToPawnTimeout)
				{
					return;
				}
			}
			else if (_actor.isOnGeodataPath())
			{
				// minimum time to calculate new route is 2 seconds
				if (GameTimeController.getInstance().getGameTicks() < (_moveToPawnTimeout + 10))
				{
					return;
				}
			}
		}
		
		// Set AI movement data
		_clientMoving = true;
		_clientMovingToPawnOffset = offset;
		_target = pawn;
		_moveToPawnTimeout = GameTimeController.getInstance().getGameTicks();
		_moveToPawnTimeout += 1000 / GameTimeController.MILLIS_IN_TICK;
		
		if (pawn == null)
		{
			return;
		}
		// Calculate movement data for a move to location action and add the actor to movingObjects of GameTimeController
		_actor.moveToLocation(pawn.getX(), pawn.getY(), pawn.getZ(), offset, calculateDistance, calculateCollisionRadius);
		
		if (!_actor.isMoving())
		{
			clientActionFailed();
			return;
		}
		
		// Send a Server->Client packet MoveToPawn/CharMoveToLocation to the actor and all L2PcInstance in its _knownPlayers
		if (pawn instanceof L2Character)
		{
			_actor.broadcastPacket(new MoveToPawn(_actor, (L2Character) pawn, offset));
		}
		else
		{
			_actor.broadcastPacket(new MoveToLocation(_actor));
		}
	}
	
	protected void moveTo(Location loc, int range, boolean calculateDistance, boolean calculateCollisionRadius)
	{
		moveTo(loc.getX(), loc.getY(), loc.getZ(), range, calculateDistance, calculateCollisionRadius);
	}
	
	protected void moveTo(Location loc)
	{
		moveTo(loc.getX(), loc.getY(), loc.getZ(), 0, false, false);
	}
	
	protected void moveTo(int x, int y, int z)
	{
		moveTo(x, y, z, 0, false, false);
	}
	
	protected void moveTo(double x, double y, double z)
	{
		moveTo(x, y, z);
	}
	
	protected void moveTo(int x, int y, int z, int range, boolean calculateDistance, boolean calculateCollisionRadius)
	{
		if ((_actor.isMovementDisabled() || (_actor.getMoveSpeed() <= 0)))
		{
			return;
		}
		
		// Set AI movement data
		_clientMoving = true;
		_clientMovingToPawnOffset = 0;
		
		// Calculate movement data for a move to location action and add the actor to movingObjects of GameTimeController
		_actor.moveToLocation(x, y, z, range, calculateDistance, calculateCollisionRadius);
		
		// Send a Server->Client packet CharMoveToLocation to the actor and all L2PcInstance in its _knownPlayers
		_actor.broadcastPacket(new MoveToLocation(_actor));
	}
	
	/**
	 * Stop the actor movement server side AND client side by sending Server->Client packet StopMove/StopRotation <I>(broadcast)</I>.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 * @param pos
	 */
	public void clientStopMoving(Location pos)
	{
		// Stop movement of the L2Character
		if (_actor.isMoving())
		{
			_actor.stopMove(pos);
		}
		
		_clientMovingToPawnOffset = 0;
		
		if (_clientMoving || (pos != null))
		{
			_clientMoving = false;
			
			// Send a Server->Client packet StopMove to the actor and all L2PcInstance in its _knownPlayers
			_actor.broadcastPacket(new StopMove(_actor));
			
			if (pos != null)
			{
				// Send a Server->Client packet StopRotation to the actor and all L2PcInstance in its _knownPlayers
				_actor.broadcastPacket(new StopRotation(_actor.getObjectId(), pos.getHeading(), 0));
			}
		}
	}
	
	/**
	 * Client has already arrived to target, no need to force StopMove packet.
	 */
	protected void clientStoppedMoving()
	{
		if (_clientMovingToPawnOffset > 0) // movetoPawn needs to be stopped
		{
			_clientMovingToPawnOffset = 0;
			_actor.broadcastPacket(new StopMove(_actor));
		}
		_clientMoving = false;
	}
	
	public boolean isAutoAttacking()
	{
		return _clientAutoAttacking;
	}
	
	public void setAutoAttacking(boolean isAutoAttacking)
	{
		if (_actor instanceof L2Summon)
		{
			L2Summon summon = (L2Summon) _actor;
			if (summon.getOwner() != null)
			{
				summon.getOwner().getAI().setAutoAttacking(isAutoAttacking);
			}
			return;
		}
		_clientAutoAttacking = isAutoAttacking;
	}
	
	/**
	 * Start the actor Auto Attack client side by sending Server->Client packet AutoAttackStart <I>(broadcast)</I>.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 */
	public void clientStartAutoAttack()
	{
		if (_actor instanceof L2Summon)
		{
			L2Summon summon = (L2Summon) _actor;
			if (summon.getOwner() != null)
			{
				summon.getOwner().getAI().clientStartAutoAttack();
			}
			return;
		}
		if (!isAutoAttacking())
		{
			if (_actor.isPlayer() && _actor.hasSummon())
			{
				_actor.getSummon().broadcastPacket(new AutoAttackStart(_actor.getSummon().getObjectId()));
			}
			// Send a Server->Client packet AutoAttackStart to the actor and all L2PcInstance in its _knownPlayers
			_actor.broadcastPacket(new AutoAttackStart(_actor.getObjectId()));
			setAutoAttacking(true);
		}
		AttackStanceTaskManager.getInstance().addAttackStanceTask(_actor);
	}
	
	/**
	 * Stop the actor auto-attack client side by sending Server->Client packet AutoAttackStop <I>(broadcast)</I>.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 */
	public void clientStopAutoAttack()
	{
		if (_actor instanceof L2Summon)
		{
			L2Summon summon = (L2Summon) _actor;
			if (summon.getOwner() != null)
			{
				summon.getOwner().getAI().clientStopAutoAttack();
			}
			return;
		}
		if (_actor instanceof L2PcInstance)
		{
			if (!AttackStanceTaskManager.getInstance().hasAttackStanceTask(_actor) && isAutoAttacking())
			{
				AttackStanceTaskManager.getInstance().addAttackStanceTask(_actor);
			}
		}
		else if (isAutoAttacking())
		{
			_actor.broadcastPacket(new AutoAttackStop(_actor.getObjectId()));
			setAutoAttacking(false);
		}
	}
	
	/**
	 * Kill the actor client side by sending Server->Client packet AutoAttackStop, StopMove/StopRotation, Die <I>(broadcast)</I>.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 */
	protected void clientNotifyDead()
	{
		// Send a Server->Client packet Die to the actor and all L2PcInstance in its _knownPlayers
		Die msg = new Die(_actor);
		_actor.broadcastPacket(msg);
		
		// Init AI
		_intention = AI_INTENTION_IDLE;
		_target = null;
		_castTarget = null;
		_attackTarget = null;
		
		// Cancel the follow task if necessary
		stopFollow();
	}
	
	/**
	 * Update the state of this actor client side by sending Server->Client packet MoveToPawn/CharMoveToLocation and AutoAttackStart to the L2PcInstance player.<br>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : Low level function, used by AI subclasses</B></FONT>
	 * @param player The L2PcIstance to notify with state of this L2Character
	 */
	public void describeStateToPlayer(L2PcInstance player)
	{
		if (getActor().isVisibleFor(player))
		{
			if (_clientMoving)
			{
				if ((_clientMovingToPawnOffset != 0) && (_followTarget != null))
				{
					// Send a Server->Client packet MoveToPawn to the actor and all L2PcInstance in its _knownPlayers
					player.moveToPawn(_actor, (L2Character) _followTarget, _clientMovingToPawnOffset);
				}
				else
				{
					// Send a Server->Client packet CharMoveToLocation to the actor and all L2PcInstance in its _knownPlayers
					player.sendPacket(new MoveToLocation(_actor));
					
					player.sendPacket(new ValidateLocation(_actor));
				}
			}
		}
	}
	
	/**
	 * Create and Launch an AI Follow Task to execute every 1s.
	 * @param target The L2Character to follow
	 */
	public synchronized void startFollow(L2Character target)
	{
		if (_followTask != null)
		{
			_followTask.cancel(false);
			_followTask = null;
		}
		
		_followTarget = target;
		_followTask = ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new FollowTask(), 5, 1000);
	}
	
	/**
	 * Stop an AI Follow Task.
	 */
	public synchronized void stopFollow()
	{
		if (_followTask != null)
		{
			// Stop the Follow Task
			_followTask.cancel(false);
			_followTask = null;
		}
		_followTarget = null;
	}
	
	protected L2Object getFollowTarget()
	{
		return _followTarget;
	}
	
	protected L2Object getTarget()
	{
		return _target;
	}
	
	protected void setTarget(L2Object target)
	{
		_target = target;
	}
	
	/**
	 * Stop all Ai tasks and futures.
	 */
	public void stopAITask()
	{
		stopFollow();
	}
	
	@Override
	public String toString()
	{
		return "Actor: " + _actor;
	}
}