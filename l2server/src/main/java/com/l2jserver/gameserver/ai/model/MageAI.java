package com.l2jserver.gameserver.ai.model;

import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_ACTIVE;

import java.util.List;

import com.l2jserver.Config;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.ai.L2AttackableAI;
import com.l2jserver.gameserver.enums.AISkillScope;
import com.l2jserver.gameserver.enums.AIType;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2GrandBossInstance;
import com.l2jserver.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jserver.gameserver.model.actor.instance.L2RaidBossInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.util.Rnd;

/**
 * @author vGodFather
 */
public class MageAI extends L2AttackableAI
{
	public MageAI(L2Attackable creature)
	{
		super(creature);
	}
	
	@Override
	protected void thinkAttack()
	{
		final L2Attackable npc = getActiveChar();
		
		if (npc.isCastingNow() || npc.isAttackingNow())
		{
			return;
		}
		
		// Delay used in order to find target again after removeTarget effect applied
		if (npc.getFindTargetDelay() > System.currentTimeMillis())
		{
			return;
		}
		
		if (npc.isCoreAIDisabled())
		{
			return;
		}
		
		final L2Character mostHate = npc.getMostHated();
		if (mostHate == null)
		{
			setIntention(AI_INTENTION_ACTIVE);
			return;
		}
		
		setAttackTarget(mostHate);
		npc.setTarget(mostHate);
		
		// Immobilize condition
		if (npc.isMovementDisabled())
		{
			movementDisable();
			return;
		}
		
		// Check if target is dead or if timeout is expired to stop this attack
		final L2Character originalAttackTarget = getAttackTarget();
		if ((originalAttackTarget == null) || originalAttackTarget.isAlikeDead() || (_attackTimeout < GameTimeController.getInstance().getGameTicks()))
		{
			// Stop hating this target after the attack timeout or if target is dead
			if (originalAttackTarget != null)
			{
				npc.stopHating(originalAttackTarget);
			}
			
			// Set the AI Intention to ACTIVE
			setIntention(AI_INTENTION_ACTIVE);
			
			npc.setWalking();
			return;
		}
		
		// Initialize data
		final int collision = npc.getTemplate().getCollisionRadius();
		
		// BOSS/Raid Minion Target Reconsider
		if (npc.isRaid() || npc.isRaidMinion())
		{
			_chaosTime++;
			if (npc instanceof L2RaidBossInstance)
			{
				if (!((L2MonsterInstance) npc).hasMinions())
				{
					if (_chaosTime > Config.RAID_CHAOS_TIME)
					{
						if (Rnd.get(100) <= (100 - ((npc.getCurrentHp() * 100) / npc.getMaxHp())))
						{
							aggroReconsider();
							_chaosTime = 0;
							return;
						}
					}
				}
				else
				{
					if (_chaosTime > Config.RAID_CHAOS_TIME)
					{
						if (Rnd.get(100) <= (100 - ((npc.getCurrentHp() * 200) / npc.getMaxHp())))
						{
							aggroReconsider();
							_chaosTime = 0;
							return;
						}
					}
				}
			}
			else if (npc instanceof L2GrandBossInstance)
			{
				if (_chaosTime > Config.GRAND_CHAOS_TIME)
				{
					double chaosRate = 100 - ((npc.getCurrentHp() * 300) / npc.getMaxHp());
					if (((chaosRate <= 10) && (Rnd.get(100) <= 10)) || ((chaosRate > 10) && (Rnd.get(100) <= chaosRate)))
					{
						aggroReconsider();
						_chaosTime = 0;
						return;
					}
				}
			}
			else
			{
				if (_chaosTime > Config.MINION_CHAOS_TIME)
				{
					if (Rnd.get(100) <= (100 - ((npc.getCurrentHp() * 200) / npc.getMaxHp())))
					{
						aggroReconsider();
						_chaosTime = 0;
						return;
					}
				}
			}
		}
		
		final List<Skill> generalSkills = npc.getTemplate().getAISkills(AISkillScope.GENERAL);
		if (!generalSkills.isEmpty())
		{
			// Heal Condition
			final List<Skill> aiHealSkills = npc.getTemplate().getAISkills(AISkillScope.HEAL);
			generalSkills.removeAll(aiHealSkills);
			if (!aiHealSkills.isEmpty())
			{
				double percentage = (npc.getCurrentHp() / npc.getMaxHp()) * 100;
				if (npc.isMinion())
				{
					L2Character leader = npc.getLeader();
					if ((leader != null) && !leader.isDead() && (Rnd.get(100) > ((leader.getCurrentHp() / leader.getMaxHp()) * 100)))
					{
						for (Skill healSkill : aiHealSkills)
						{
							if (!checkSkillCastConditions(npc, healSkill))
							{
								continue;
							}
							
							if (GeoData.getInstance().canSeeTarget(npc, leader))
							{
								clientStopMoving(null);
								final L2Object target = npc.getTarget();
								npc.setTarget(leader);
								npc.doCast(healSkill);
								npc.setTarget(target);
								LOG.debug(this + " used heal skill " + healSkill + " on leader {}", leader);
								return;
							}
						}
					}
				}
				
				if (Rnd.get(100) < ((100 - percentage) / 3))
				{
					for (Skill sk : aiHealSkills)
					{
						if (!checkSkillCastConditions(npc, sk))
						{
							continue;
						}
						
						clientStopMoving(null);
						final L2Object target = npc.getTarget();
						npc.setTarget(npc);
						npc.doCast(sk);
						npc.setTarget(target);
						LOG.debug("{} used heal skill {} on itself", this, sk);
						return;
					}
				}
				
				for (Skill sk : aiHealSkills)
				{
					if (!checkSkillCastConditions(npc, sk))
					{
						continue;
					}
				}
			}
			
			// Res Skill Condition
			final List<Skill> aiResSkills = npc.getTemplate().getAISkills(AISkillScope.RES);
			generalSkills.removeAll(aiResSkills);
			if (!aiResSkills.isEmpty())
			{
				if (npc.isMinion())
				{
					L2Character leader = npc.getLeader();
					if ((leader != null) && leader.isDead())
					{
						for (Skill sk : aiResSkills)
						{
							if (!checkSkillCastConditions(npc, sk))
							{
								continue;
							}
							
							if (GeoData.getInstance().canSeeTarget(npc, leader))
							{
								clientStopMoving(null);
								final L2Object target = npc.getTarget();
								npc.setTarget(leader);
								npc.doCast(sk);
								npc.setTarget(target);
								LOG.debug(this + " used resurrection skill " + sk + " on leader {}", leader);
								return;
							}
						}
					}
				}
				
				for (Skill sk : aiResSkills)
				{
					if (!checkSkillCastConditions(npc, sk))
					{
						continue;
					}
				}
			}
			
			if (npc.getAiType() == AIType.MAGE)
			{
				for (Skill skill : generalSkills)
				{
					if (!skill.isPassive() && (skill.isBad()))
					{
						if (!checkSkillCastConditions(npc, skill))
						{
							continue;
						}
						
						if (maybeMoveToPawn(npc.getTarget(), _actor.getMagicalAttackRange(skill)))
						{
							return;
						}
						
						clientStopMoving(null);
						npc.doCast(skill);
						return;
					}
				}
			}
		}
		
		double dist = Math.sqrt(npc.getPlanDistanceSq(mostHate.getX(), mostHate.getY()));
		int dist2 = (int) dist - collision;
		
		// Long/Short Range skill usage.
		if (!npc.getShortRangeSkills().isEmpty() && npc.hasSkillChance() && (dist2 <= 150))
		{
			final Skill shortRangeSkill = npc.getShortRangeSkills().get(Rnd.get(npc.getShortRangeSkills().size()));
			if (checkSkillCastConditions(npc, npc.getTarget(), shortRangeSkill))
			{
				clientStopMoving(null);
				npc.doCast(shortRangeSkill);
				LOG.debug(this + " used short range skill " + shortRangeSkill + " on {}", npc.getTarget());
				return;
			}
		}
		
		if (!npc.getLongRangeSkills().isEmpty() && npc.hasSkillChance() && (dist2 > 150))
		{
			final Skill longRangeSkill = npc.getLongRangeSkills().get(Rnd.get(npc.getLongRangeSkills().size()));
			if (checkSkillCastConditions(npc, npc.getTarget(), longRangeSkill))
			{
				clientStopMoving(null);
				npc.doCast(longRangeSkill);
				LOG.debug(this + " used long range skill " + longRangeSkill + " on {}", npc.getTarget());
				return;
			}
		}
		
		if (maybeMoveToPawn(getAttackTarget(), _actor.getPhysicalAttackRange()))
		{
			return;
		}
		
		clientStopMoving(null);
		// Attacks target
		_actor.doAttack(getAttackTarget());
	}
}