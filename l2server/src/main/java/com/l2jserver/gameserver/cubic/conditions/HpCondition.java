/*
 * Copyright (C) 2004-2015 L2J Unity
 * 
 * This file is part of L2J Unity.
 * 
 * L2J Unity is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Unity is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.cubic.conditions;

import com.l2jserver.gameserver.cubic.CubicInstance;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;

/**
 * @author UnAfraid
 */
public class HpCondition implements ICubicCondition
{
	private final HpConditionType _type;
	private final int _hpPer;
	
	public HpCondition(HpConditionType type, int hpPer)
	{
		_type = type;
		_hpPer = hpPer;
	}
	
	@Override
	public boolean test(CubicInstance cubic, L2Character owner, L2Object target)
	{
		if (owner.isCharacter())
		{
			final double hpPer = owner.getCurrentHpPercent();
			switch (_type)
			{
				case GREATER: // activate skill if cubic template percent value is greater than target's hp percent
				{
					return hpPer > _hpPer;
				}
				case LESSER: // activate skill if cubic template percent value is lesser than target's hp percent
				{
					return hpPer < _hpPer;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + " chance: " + _hpPer;
	}
	
	public static enum HpConditionType
	{
		GREATER,
		LESSER;
	}
}
