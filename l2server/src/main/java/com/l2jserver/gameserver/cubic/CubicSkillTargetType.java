/*
 * Copyright (C) L2J Sunrise
 * This file is part of L2J Sunrise.
 */
package com.l2jserver.gameserver.cubic;

public enum CubicSkillTargetType
{
	HEAL,
	MASTER,
	TARGET
}