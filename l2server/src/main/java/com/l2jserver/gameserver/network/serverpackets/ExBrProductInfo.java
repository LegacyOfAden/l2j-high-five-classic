package com.l2jserver.gameserver.network.serverpackets;

import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.model.primeshop.L2ProductItem;
import com.l2jserver.gameserver.model.primeshop.L2ProductItemComponent;

/**
 * Created by GodFather
 */
public class ExBrProductInfo extends L2GameServerPacket
{
	private final L2ProductItem _productId;
	
	public ExBrProductInfo(int id)
	{
		_productId = ProductItemData.getInstance().getProduct(id);
	}
	
	@Override
	protected void writeImpl()
	{
		if (_productId == null)
		{
			return;
		}
		
		writeC(0xFE);
		writeH(0xD7);
		
		writeD(_productId.getProductId());
		writeD(_productId.getPoints());
		writeD(_productId.getComponents().size());
		
		for (L2ProductItemComponent com : _productId.getComponents())
		{
			writeD(com.getId());
			writeD(com.getCount());
			writeD(com.getWeight());
			writeD(com.isDropable() ? 1 : 0);
		}
	}
}