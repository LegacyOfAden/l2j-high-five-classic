package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Created by GodFather
 */
public class RequestBrBuyProduct extends L2GameClientPacket
{
	private static final String TYPE = "[C] D0 68 RequestBrBuyProduct";
	
	private int _productId;
	private int _count;
	
	@Override
	protected void readImpl()
	{
		_productId = readD();
		_count = readD();
	}
	
	@Override
	protected void runImpl()
	{
		L2PcInstance player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		ProductItemData.getInstance().requestBuyItem(player, _productId, _count);
	}
	
	@Override
	public String getType()
	{
		return TYPE;
	}
}