package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Created by GodFather
 */
public class RequestBrRecentProductList extends L2GameClientPacket
{
	@Override
	public void readImpl()
	{
	}
	
	@Override
	public void runImpl()
	{
		L2PcInstance player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		ProductItemData.getInstance().recentProductList(player);
	}
	
	@Override
	public String getType()
	{
		return null;
	}
}