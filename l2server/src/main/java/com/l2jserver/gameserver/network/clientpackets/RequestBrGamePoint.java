package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.ExBrGamePoint;

/**
 * Created by GodFather
 */
public class RequestBrGamePoint extends L2GameClientPacket
{
	private static final String TYPE = "[C] D0:65 RequestBrGamePoint";
	
	@Override
	protected void readImpl()
	{
	}
	
	@Override
	protected void runImpl()
	{
		L2PcInstance player = getClient().getActiveChar();
		
		if (player == null)
		{
			return;
		}
		
		player.sendPacket(new ExBrGamePoint(player));
	}
	
	@Override
	public String getType()
	{
		return TYPE;
	}
}