/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.datatables;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.InstanceListManager;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.util.data.xml.IXmlReader;
import com.sun.media.jfxmedia.logging.Logger;

/**
 * @author Kara`
 */
public class MerchantPriceConfigTable implements InstanceListManager, IXmlReader
{
	private final Map<Integer, MerchantPriceConfig> _mpcs = new ConcurrentHashMap<>();
	
	public MerchantPriceConfigTable()
	{
		load();
	}
	
	@Override
	public void load()
	{
		parseDatapackFile("data/xml/other/MerchantPriceConfig.xml");
		Logger.logMsg(Level.FINE.intValue(), "[" + MerchantPriceConfigTable.class.getSimpleName() + "]: Loaded: " + _mpcs.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("merchantPriceConfig".equalsIgnoreCase(n.getNodeName()))
			{
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("priceConfig".equalsIgnoreCase(d.getNodeName()))
					{
						int id = 0;
						int baseTax = 0;
						int castleId = -1;
						int zoneId = -1;
						String name = "";
						
						NamedNodeMap atr = d.getAttributes();
						
						if (atr.getNamedItem("id") != null)
						{
							id = Integer.parseInt(atr.getNamedItem("id").getNodeValue());
						}
						
						if (atr.getNamedItem("name") != null)
						{
							name = atr.getNamedItem("name").getNodeValue();
						}
						
						if (atr.getNamedItem("baseTax") != null)
						{
							baseTax = Integer.parseInt(atr.getNamedItem("baseTax").getNodeValue());
						}
						
						if (atr.getNamedItem("castleId") != null)
						{
							castleId = Integer.parseInt(atr.getNamedItem("castleId").getNodeValue());
						}
						
						if (atr.getNamedItem("zoneId") != null)
						{
							zoneId = Integer.parseInt(atr.getNamedItem("zoneId").getNodeValue());
						}
						
						_mpcs.put(id, new MerchantPriceConfig(id, name, baseTax, castleId, zoneId));
					}
				}
			}
		}
	}
	
	public MerchantPriceConfig getMerchantPriceConfig(L2MerchantInstance npc)
	{
		for (MerchantPriceConfig mpc : _mpcs.values())
		{
			if ((npc.getWorldRegion() != null) && npc.getWorldRegion().containsZone(mpc.getZoneId()))
			{
				return mpc;
			}
		}
		return null;
	}
	
	public MerchantPriceConfig getMerchantPriceConfig(int id)
	{
		return _mpcs.get(id);
	}
	
	@Override
	public void loadInstances()
	{
		try
		{
			load();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateReferences()
	{
		for (final MerchantPriceConfig mpc : _mpcs.values())
		{
			mpc.updateReferences();
		}
	}
	
	@Override
	public void activateInstances()
	{
	}
	
	/**
	 * @author KenM
	 */
	public static final class MerchantPriceConfig
	{
		private final int _id;
		private final String _name;
		private final int _baseTax;
		private final int _castleId;
		private Castle _castle;
		private final int _zoneId;
		
		public MerchantPriceConfig(final int id, final String name, final int baseTax, final int castleId, final int zoneId)
		{
			_id = id;
			_name = name;
			_baseTax = baseTax;
			_castleId = castleId;
			_zoneId = zoneId;
		}
		
		/**
		 * @return Returns the id.
		 */
		public int getId()
		{
			return _id;
		}
		
		/**
		 * @return Returns the name.
		 */
		public String getName()
		{
			return _name;
		}
		
		/**
		 * @return Returns the baseTax.
		 */
		public int getBaseTax()
		{
			return _baseTax;
		}
		
		/**
		 * @return Returns the baseTax / 100.0.
		 */
		public double getBaseTaxRate()
		{
			return _baseTax / 100.0;
		}
		
		/**
		 * @return Returns the castle.
		 */
		public Castle getCastle()
		{
			return _castle;
		}
		
		/**
		 * @return Returns the zoneId.
		 */
		public int getZoneId()
		{
			return _zoneId;
		}
		
		public boolean hasCastle()
		{
			return getCastle() != null;
		}
		
		public double getCastleTaxRate()
		{
			return hasCastle() ? getCastle().getTaxRate() : 0.0;
		}
		
		public int getTotalTax()
		{
			return hasCastle() ? (getCastle().getTaxPercent() + getBaseTax()) : getBaseTax();
		}
		
		public double getTotalTaxRate()
		{
			return getTotalTax() / 100.0;
		}
		
		public void updateReferences()
		{
			if (_castleId > 0)
			{
				_castle = CastleManager.getInstance().getCastleById(_castleId);
			}
		}
	}
	
	public static MerchantPriceConfigTable getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static class SingletonHolder
	{
		protected static final MerchantPriceConfigTable _instance = new MerchantPriceConfigTable();
	}
}
