package com.l2jserver.gameserver.votereward;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.util.data.xml.IXmlReader;

public class VoteXmlTable implements IXmlReader
{
	private String _apiKeyNet;
	private String _apiKeyHop;
	private String _apiKeyTop;
	public int _apiTopServerid;
	
	private final RewardList _rewardlist = new RewardList();
	
	public VoteXmlTable()
	{
		load();
	}
	
	@Override
	public synchronized void load()
	{
		_rewardlist.getDrops().clear();
		// _site.clear();
		parseDatapackFile("data/xml/other/voteSettings.xml");
		LOG.info(getClass().getSimpleName() + ": Loaded " + _rewardlist.getGroups().size() + " rewards.");
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		NamedNodeMap attrs;
		for (Node a = doc.getFirstChild(); a != null; a = a.getNextSibling())
		{
			if ("list".equalsIgnoreCase(a.getNodeName()))
			{
				for (Node b = a.getFirstChild(); b != null; b = b.getNextSibling())
				{
					switch (b.getNodeName())
					{
						case "apiNetwork":
						{
							attrs = b.getAttributes();
							_apiKeyNet = parseString(attrs, "apiNetworkKey");
							break;
						}
						case "apiTopzone":
						{
							attrs = b.getAttributes();
							_apiKeyTop = parseString(attrs, "apiTopzoneKey");
							_apiTopServerid = parseInteger(attrs, "apiTopzoneId");
							break;
						}
						case "apiHopzone":
						{
							attrs = b.getAttributes();
							_apiKeyHop = parseString(attrs, "apiHopzoneKey");
							break;
						}
						case "rewards":
						{
							for (Node rewardsNode = b.getFirstChild(); rewardsNode != null; rewardsNode = rewardsNode.getNextSibling())
							{
								switch (rewardsNode.getNodeName())
								{
									case "group":
									{
										attrs = rewardsNode.getAttributes();
										final float groupChance = parseFloat(attrs, "chance");
										final RewardGroup group = new RewardGroup(groupChance);
										for (Node z = rewardsNode.getFirstChild(); z != null; z = z.getNextSibling())
										{
											switch (z.getNodeName())
											{
												case "item":
												{
													attrs = z.getAttributes();
													final int itemId = parseInteger(attrs, "id");
													final int min = parseInteger(attrs, "min");
													final int max = parseInteger(attrs, "max");
													final float chance = parseFloat(attrs, "chance");
													group.addItem(new RewardItem(itemId, min, max, chance));
													break;
												}
											}
										}
										_rewardlist.addGroup(group);
										break;
									}
								}
							}
							break;
						}
					}
				}
			}
		}
	}
	
	public RewardList getDroplist()
	{
		return _rewardlist;
	}
	
	public String getAPIKeyTop()
	{
		return _apiKeyTop;
	}
	
	public String getAPIKeyHop()
	{
		return _apiKeyHop;
	}
	
	public String getAPIKeyNet()
	{
		return _apiKeyNet;
	}
	
	public int getAPITopzoneId()
	{
		return _apiTopServerid;
	}
	
	public static VoteXmlTable getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static class SingletonHolder
	{
		protected static final VoteXmlTable _instance = new VoteXmlTable();
	}
}
