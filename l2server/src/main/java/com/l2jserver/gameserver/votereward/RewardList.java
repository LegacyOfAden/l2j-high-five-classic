package com.l2jserver.gameserver.votereward;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.l2jserver.util.Rnd;

public class RewardList
{
	private List<RewardGroup> _groups;
	
	public void addGroup(RewardGroup group)
	{
		if (_groups == null)
		{
			_groups = new ArrayList<>();
		}
		
		_groups.add(group);
	}
	
	public List<RewardGroup> getGroups()
	{
		return _groups != null ? _groups : Collections.<RewardGroup> emptyList();
	}
	
	public boolean hasDrops()
	{
		return (_groups != null) && !_groups.isEmpty();
	}
	
	public List<RewardGroup> getDrops()
	{
		return _groups != null ? _groups : Collections.<RewardGroup> emptyList();
	}
	
	public List<RewardItem> calculateDrops()
	{
		List<RewardItem> itemsToDrop = null;
		for (RewardGroup group : _groups)
		{
			final double groupRandom = 100 * Rnd.nextDouble();
			if (groupRandom < (group.getChance()))
			{
				final double itemRandom = 100 * Rnd.nextDouble();
				float cumulativeChance = 0;
				for (RewardItem item : group.getItems())
				{
					if (itemRandom < (cumulativeChance += item.getChance()))
					{
						if (itemsToDrop == null)
						{
							itemsToDrop = new ArrayList<>();
						}
						
						itemsToDrop.add(item);
						break;
					}
				}
			}
		}
		return itemsToDrop != null ? itemsToDrop : Collections.<RewardItem> emptyList();
	}
	
	public void clear()
	{
		_groups.clear();
	}
}