package com.l2jserver.gameserver.votereward;

import com.l2jserver.util.Rnd;

public class RewardItem
{
	private final int _itemId;
	private final int _min;
	private final int _max;
	private final double _chance;
	
	public RewardItem(int itemId, int min, int max, double chance)
	{
		_itemId = itemId;
		_min = min;
		_max = max;
		_chance = chance;
	}
	
	public int getId()
	{
		return _itemId;
	}
	
	public int getCount()
	{
		return Rnd.get(_min, _max);
	}
	
	public int getMin()
	{
		return _min;
	}
	
	public int getMax()
	{
		return _max;
	}
	
	public double getChance()
	{
		return _chance;
	}
}