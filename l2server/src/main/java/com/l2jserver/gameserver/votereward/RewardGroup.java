package com.l2jserver.gameserver.votereward;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RewardGroup
{
	private final double _chance;
	private List<RewardItem> _items;
	
	public RewardGroup(float chance)
	{
		_chance = chance;
	}
	
	public double getChance()
	{
		return _chance;
	}
	
	public List<RewardItem> getItems()
	{
		return _items != null ? _items : Collections.<RewardItem> emptyList();
	}
	
	public void addItem(RewardItem item)
	{
		if (_items == null)
		{
			_items = new ArrayList<>();
		}
		_items.add(item);
	}
}