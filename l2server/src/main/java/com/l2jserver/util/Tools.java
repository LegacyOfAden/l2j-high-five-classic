package com.l2jserver.util;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public final class Tools
{
	protected static final Logger _log = org.slf4j.LoggerFactory.getLogger(Tools.class);
	
	public Tools()
	{
	}
	
	public static String convertDateToString(long time)
	{
		Date dt = new Date(time);
		SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String stringDate = s.format(dt);
		return stringDate;
	}
	
	public static String convertHourToString(long time)
	{
		Date dt = new Date(time);
		SimpleDateFormat s = new SimpleDateFormat("HH:mm");
		String stringDate = s.format(dt);
		return stringDate;
	}
	
	public static String convertMinuteToString(long time)
	{
		Date dt = new Date(time);
		SimpleDateFormat s = new SimpleDateFormat("mm:ss");
		String stringDate = s.format(dt);
		return stringDate;
	}
	
	public static boolean isDualBox(L2PcInstance player1, L2PcInstance player2)
	{
		return isDualBox(player1, player2, false);
	}
	
	public static boolean isDualBox(L2PcInstance player1, L2PcInstance player2, boolean recordDualBox)
	{
		if ((player1.getClient() == null) || (player1.getClient().isDetached()) || (player2.getClient() == null) || (player2.getClient().isDetached()))
		{
			return false;
		}
		
		try
		{
			String ip_net1 = player1.getClient().getConnection().getInetAddress().getHostAddress();
			String ip_net2 = player2.getClient().getConnection().getInetAddress().getHostAddress();
			
			String ip_pc1 = "";
			String ip_pc2 = "";
			int[][] trace1 = player1.getClient().getTrace();
			for (int o = 0; o < trace1[0].length; o++)
			{
				ip_pc1 = ip_pc1 + trace1[0][o];
				if (o != (trace1[0].length - 1))
				{
					ip_pc1 = ip_pc1 + ".";
				}
			}
			int[][] trace2 = player2.getClient().getTrace();
			for (int u = 0; u < trace2[0].length; u++)
			{
				ip_pc2 = ip_pc2 + trace2[0][u];
				if (u != (trace2[0].length - 1))
				{
					ip_pc2 = ip_pc2 + ".";
				}
			}
			
			if ((ip_net1.equals(ip_net2)) && (ip_pc1.equals(ip_pc2)))
			{
				if (recordDualBox)
				{
					_log.warn("Dual Box System: " + player1 + " (" + ip_net1 + "/ " + ip_pc1 + ") Dual Box Detected!");
					_log.warn("Dual Box System: " + player2 + " (" + ip_net2 + "/ " + ip_pc2 + ") Dual Box Detected!");
				}
				return true;
			}
			
		}
		catch (Exception e)
		{
			return false;
		}
		
		return false;
	}
	
	public static final void sleep(long time)
	{
		try
		{
			Thread.sleep(time);
		}
		catch (InterruptedException localInterruptedException)
		{
		}
	}
}