/*
 * Copyright (C) 2004-2016 L2J Unity
 * 
 * This file is part of L2J Unity.
 * 
 * L2J Unity is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Unity is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.pump;

import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.stat.CharStat;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Stats;
import com.l2jserver.gameserver.model.stats.functions.FuncMul;

/**
 * @author FinalDestination
 */
public class p_reuse_delay extends AbstractEffect
{
	private final int _magic;
	private final double _amount;
	
	public p_reuse_delay(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_magic = params.getInt("magic", 0);
		_amount = 1 + (params.getInt("amount", 0) / 100.0);
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		final L2Character effected = info.getEffected();
		final CharStat charStat = effected.getStat();
		
		switch (_magic)
		{
			case 0:
			{
				charStat.getActiveChar().addStatFunc(new FuncMul(Stats.P_REUSE, 1, this, _amount, null));
				break;
			}
			case 1:
			{
				charStat.getActiveChar().addStatFunc(new FuncMul(Stats.MAGIC_REUSE, 1, this, _amount, null));
				break;
			}
			case 2:
			{
				charStat.getActiveChar().addStatFunc(new FuncMul(Stats.STATIC_REUSE, 1, this, _amount, null));
				break;
			}
			case 3:
			{
				charStat.getActiveChar().addStatFunc(new FuncMul(Stats.DANCE_REUSE, 1, this, _amount, null));
				break;
			}
		}
	}
}