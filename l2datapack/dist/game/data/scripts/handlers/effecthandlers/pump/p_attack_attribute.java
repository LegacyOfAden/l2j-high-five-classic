/*
 * Copyright (C) 2004-2016 L2J Unity
 * 
 * This file is part of L2J Unity.
 * 
 * L2J Unity is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Unity is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.pump;

import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.stat.CharStat;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.AttributeType;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Stats;
import com.l2jserver.gameserver.model.stats.functions.FuncAdd;

/**
 * @author FinalDestination
 */
public class p_attack_attribute extends AbstractEffect
{
	private final AttributeType _slot;
	private final double _amount;
	
	public p_attack_attribute(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_slot = params.getEnum("slot", AttributeType.class, AttributeType.FIRE);
		_amount = params.getDouble("amount", 0);
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		final L2Character effected = info.getEffected();
		final CharStat charStat = effected.getStat();
		
		switch (_slot)
		{
			case FIRE:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.FIRE_POWER, 1, this, _amount, null));
				break;
			}
			case WATER:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.WATER_POWER, 1, this, _amount, null));
				break;
			}
			case WIND:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.WIND_POWER, 1, this, _amount, null));
				break;
			}
			case EARTH:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.EARTH_POWER, 1, this, _amount, null));
				break;
			}
			case HOLY:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.HOLY_POWER, 1, this, _amount, null));
				break;
			}
			case DARK:
			{
				charStat.getActiveChar().addStatFunc(new FuncAdd(Stats.DARK_POWER, 1, this, _amount, null));
				break;
			}
		}
	}
}