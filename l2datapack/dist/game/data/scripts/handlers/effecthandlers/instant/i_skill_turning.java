/*
 * Copyright (C) 2004-2018 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Formulas;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Skill Turning effect implementation.
 */
public final class i_skill_turning extends AbstractEffect
{
	private final int _magic;
	private final int _chance;
	
	public i_skill_turning(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_magic = params.getInt("magic", 0);
		_chance = params.getInt("chance", -1);
	}
	
	@Override
	public boolean calcSuccess(BuffInfo info)
	{
		return Formulas.calcProbability(_chance, info.getEffector(), info.getEffected(), info.getSkill());
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		if ((info.getEffected() == null) || (info.getEffected() == info.getEffector()) || info.getEffected().isRaid())
		{
			return;
		}
		
		final L2Character target = info.getEffected();
		
		switch (_magic)
		{
			case 0:
			{
				if (target.isCastingNow() && target.canAbortCast() && (target.getLastSkillCast() != null) && (target.getLastSkillCast().isPhysical()))
				{
					// Abort the cast of the L2Character and send Server->Client MagicSkillCanceld/ActionFailed packet.
					target.abortCast();
					
					if (target.isPlayer())
					{
						// Send a system message
						target.sendPacket(SystemMessageId.CASTING_INTERRUPTED);
					}
				}
				break;
			}
			case 1:
			{
				if (target.isCastingNow() && target.canAbortCast() && (target.getLastSkillCast() != null) && (target.getLastSkillCast().isMagic()))
				{
					// Abort the cast of the L2Character and send Server->Client MagicSkillCanceld/ActionFailed packet.
					target.abortCast();
					
					if (target.isPlayer())
					{
						// Send a system message
						target.sendPacket(SystemMessageId.CASTING_INTERRUPTED);
					}
				}
				break;
			}
			case 2:
			{
				if (target.isCastingNow() && target.canAbortCast() && (target.getLastSkillCast() != null) && (target.getLastSkillCast().isStatic()))
				{
					// Abort the cast of the L2Character and send Server->Client MagicSkillCanceld/ActionFailed packet.
					target.abortCast();
					
					if (target.isPlayer())
					{
						// Send a system message
						target.sendPacket(SystemMessageId.CASTING_INTERRUPTED);
					}
				}
				break;
			}
			case 3:
			{
				if (target.isCastingNow() && target.canAbortCast() && (target.getLastSkillCast() != null) && (target.getLastSkillCast().isDance()))
				{
					// Abort the cast of the L2Character and send Server->Client MagicSkillCanceld/ActionFailed packet.
					target.abortCast();
					
					if (target.isPlayer())
					{
						// Send a system message
						target.sendPacket(SystemMessageId.CASTING_INTERRUPTED);
					}
				}
				break;
			}
		}
	}
}