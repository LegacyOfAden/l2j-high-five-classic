/*
 * Copyright (C) 2004-2018 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.gameserver.cubic.CubicInstance;
import com.l2jserver.gameserver.data.xml.impl.CubicData;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.CubicTemplate;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.util.Rnd;

/**
 * Summon Cubic effect implementation.
 * @author Zoey76
 */
public final class i_summon_cubic extends AbstractEffect
{
	private final int _cubicId;
	private final int _cubicLvl;
	
	public i_summon_cubic(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_cubicId = params.getInt("cubicId");
		_cubicLvl = params.getInt("cubicLvl");
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		final L2PcInstance activeChar = info.getEffector().getActingPlayer();
		if (activeChar == null)
		{
			return;
		}
		
		if ((info.getEffected() == null) || !info.getEffected().isPlayer() || info.getEffected().isAlikeDead() || info.getEffected().getActingPlayer().inObserverMode())
		{
			return;
		}
		
		if (_cubicId < 0)
		{
			_log.warning(i_summon_cubic.class.getSimpleName() + ": Invalid Cubic ID:" + _cubicId + " in skill ID: " + info.getSkill().getId());
			return;
		}
		
		final L2PcInstance target = info.getEffected().getActingPlayer();
		if (target.inObserverMode() || target.isMounted())
		{
			return;
		}
		
		// If cubic is already present, it's replaced.
		final CubicInstance cubic = target.getCubicById(_cubicId);
		if (cubic != null)
		{
			cubic.deactivate();
		}
		else
		{
			// If maximum amount is reached, random cubic is removed.
			// Players with no mastery can have only one cubic.
			final int allowedCubicCount = info.getEffected().getActingPlayer().getStat().getMaxCubicCount();
			final int currentCubicCount = target.getCubics().size();
			// Extra cubics are removed, one by one, randomly.
			for (int i = 0; i <= (currentCubicCount - allowedCubicCount); i++)
			{
				final int removedCubicId = (int) target.getCubics().keySet().toArray()[Rnd.get(currentCubicCount)];
				final CubicInstance removedCubic = target.getCubicById(removedCubicId);
				removedCubic.deactivate();
			}
		}
		
		final CubicTemplate template = CubicData.getInstance().getCubicTemplate(_cubicId, _cubicLvl);
		if (template == null)
		{
			_log.warning("Attempting to summon cubic without existing template id: {} level: {}");
			return;
		}
		
		// Adding a new cubic.
		target.addCubic(new CubicInstance(target, activeChar, template));
		target.broadcastUserInfo();
	}
}
