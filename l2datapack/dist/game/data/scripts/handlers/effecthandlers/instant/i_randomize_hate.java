/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import java.util.List;
import java.util.stream.Collectors;

import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.effects.L2EffectType;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Formulas;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.util.Rnd;

/**
 * Randomize Hate effect implementation.
 */
public final class i_randomize_hate extends AbstractEffect
{
	private final int _chance;
	
	public i_randomize_hate(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_chance = params.getInt("chance", -1);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.HATE;
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		if ((info.getEffected() == null) || (info.getEffected() == info.getEffector()) || info.getEffected().isPlayer())
		{
			return;
		}
		
		if (!Formulas.calcProbability(_chance, info.getEffector(), info.getEffected(), info.getSkill()))
		{
			info.getEffector().sendPacket(SystemMessageId.FAILED_CHANGE_TARGET);
			return;
		}
		
		if (info.getEffected().isAttackable())
		{
			final List<L2Character> aggroList = ((L2Attackable) info.getEffected()).getAggroList().keySet().stream().filter(c -> c != info.getEffector()).collect(Collectors.toList());
			if (aggroList.isEmpty())
			{
				return;
			}
			
			// Choosing randomly a new target
			final L2Character target = aggroList.get(Rnd.get(aggroList.size()));
			final long hate = ((L2Attackable) info.getEffected()).getHating(info.getEffector());
			((L2Attackable) info.getEffected()).stopHating(info.getEffector());
			((L2Attackable) info.getEffected()).addDamageHate(target, 0, hate);
		}
	}
}