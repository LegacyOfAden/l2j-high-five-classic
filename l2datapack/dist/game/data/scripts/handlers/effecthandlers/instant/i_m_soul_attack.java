/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.effects.L2EffectType;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.stats.Formulas;
import com.l2jserver.gameserver.model.stats.Stats;
import com.l2jserver.util.Rnd;

/**
 * Magical Soul Attack effect implementation.
 * @author Adry_85
 */
public final class i_m_soul_attack extends AbstractEffect
{
	private final double _power;
	private final double _shieldDefense;
	private final boolean _overHit;
	
	public i_m_soul_attack(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_power = params.getDouble("power", 0);
		_shieldDefense = params.getDouble("shieldDefense", 0);
		_overHit = params.getBoolean("overHit", false);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.MAGICAL_ATTACK;
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		final L2Character target = info.getEffected();
		final L2Character activeChar = info.getEffector();
		final Skill skill = info.getSkill();
		
		if (activeChar.isAlikeDead())
		{
			return;
		}
		
		if (target.isPlayer() && target.getActingPlayer().isFakeDeath())
		{
			target.stopFakeDeath(true);
		}
		
		if (_overHit && target.isAttackable())
		{
			((L2Attackable) target).overhitEnabled(true);
		}
		
		byte shield = 0;
		if (_shieldDefense > 0)
		{
			shield = Formulas.calcShieldSuccess(activeChar, target, skill, true);
		}
		
		boolean sps = skill.useSpiritShot() && activeChar.isChargedShot(ShotType.SPIRITSHOTS);
		boolean bss = skill.useSpiritShot() && activeChar.isChargedShot(ShotType.BLESSED_SPIRITSHOTS);
		final boolean mcrit = Formulas.calcMCrit(activeChar, target, activeChar.getMCriticalHit(target, skill));
		final int chargedSouls = Math.min(skill.getSoulConsume(), activeChar.getActingPlayer().getChargedSouls());
		double mAtk = activeChar.getMAtk(target, skill) * (chargedSouls > 0 ? (1.3 + (chargedSouls * 0.05)) : 1);
		double damage = Formulas.calcMAtkDamage(activeChar, target, skill, mAtk, shield, sps, bss, mcrit, _power, _shieldDefense);
		
		if (damage > 0)
		{
			// Manage attack or cast break of the target (calculating rate, sending message...)
			if (!target.isRaid() && Formulas.calcAtkBreak(target, damage))
			{
				target.breakAttack();
				target.breakCast();
			}
			
			if (!activeChar.isHpBlocked() && !target.isHpBlocked()) // No reflect/absorb if actor/target is invulnerable.
			{
				// Reduce HP of the target and calculate reflection damage to reduce HP of attacker if necessary (max reflect to 100%)
				double reflectPercent = Math.min(target.getStat().calcStat(Stats.REFLECT_DAMAGE_PERCENT, 0, null, null), 100);
				int reflectedDamage = 0;
				
				if (reflectPercent > 0)
				{
					reflectedDamage = (int) ((reflectPercent / 100) * damage);
					reflectedDamage = Math.min(reflectedDamage, reflectedDamage);
					activeChar.reduceCurrentHp(reflectedDamage, target, skill);
					activeChar.notifyDamageReceived(reflectedDamage, target, skill, mcrit, false, true);
				}
				
				// Absorb HP from the damage inflicted
				double absorbPercent = activeChar.getStat().calcStat(Stats.ABSORB_DAMAGE_PERCENT, 0);
				double absorbChance = activeChar.getStat().calcStat(Stats.ABSORB_DAMAGE_PERCENT_CHANCE, 0);
				
				if ((absorbPercent > 0) && (Rnd.get(100) < absorbChance))
				{
					int maxCanAbsorb = (int) (activeChar.getMaxRecoverableHp() - activeChar.getCurrentHp());
					int absorbDamage = (int) ((absorbPercent / 100) * damage);
					
					if (absorbDamage > maxCanAbsorb)
					{
						absorbDamage = maxCanAbsorb;
					}
					
					if (absorbDamage > 0)
					{
						activeChar.setCurrentHp(activeChar.getCurrentHp() + absorbDamage);
					}
				}
				
				// Absorb MP from the damage inflicted
				absorbPercent = activeChar.getStat().calcStat(Stats.ABSORB_MANA_DAMAGE_PERCENT, 0);
				
				if (absorbPercent > 0)
				{
					int maxCanAbsorb = (int) (activeChar.getMaxRecoverableMp() - activeChar.getCurrentMp());
					int absorbDamage = (int) ((absorbPercent / 100) * damage);
					
					if (absorbDamage > maxCanAbsorb)
					{
						absorbDamage = maxCanAbsorb;
					}
					
					if (absorbDamage > 0)
					{
						activeChar.setCurrentMp(activeChar.getCurrentMp() + absorbDamage);
					}
				}
			}
			
			if ((target.getStat().calcStat(Stats.VENGEANCE_SKILL_MAGIC_DAMAGE, 0, target, skill) > Rnd.get(100)) && !activeChar.isHpBlocked() && !target.isHpBlocked())
			{
				activeChar.reduceCurrentHp(damage, target, skill);
				activeChar.notifyDamageReceived(damage, target, skill, mcrit, false, true);
			}
			else
			{
				target.reduceCurrentHp(damage, activeChar, skill, mcrit);
				target.notifyDamageReceived(damage, activeChar, skill, mcrit, false, false);
				activeChar.sendDamageMessage(target, (int) damage, mcrit, false, false);
			}
		}
	}
}