/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.effects.L2EffectType;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.stats.Formulas;
import com.l2jserver.gameserver.model.stats.Stats;
import com.l2jserver.util.Rnd;

/**
 * Fatal Blow effect implementation.
 * @author Adry_85
 */
public final class i_fatal_blow extends AbstractEffect
{
	private final double _power;
	private final double _chanceBoost;
	private final double _criticalChance;
	private final boolean _overHit;
	
	public i_fatal_blow(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_power = params.getDouble("power", 0);
		_chanceBoost = params.getDouble("chanceBoost", 0);
		_criticalChance = params.getDouble("criticalChance", 0);
		_overHit = params.getBoolean("overHit", false);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.PHYSICAL_ATTACK;
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		L2Character target = info.getEffected();
		L2Character activeChar = info.getEffector();
		Skill skill = info.getSkill();
		
		if (activeChar.isAlikeDead())
		{
			return;
		}
		
		if (Formulas.calcBlowSuccess(activeChar, target, skill, _chanceBoost) && !Formulas.calcSkillEvasion(activeChar, target, skill))
		{
			if (_overHit && target.isAttackable())
			{
				((L2Attackable) target).overhitEnabled(true);
			}
			
			boolean crit = false;
			if (_criticalChance > 0)
			{
				crit = Formulas.calcSkillCrit(activeChar, target, _criticalChance);
			}
			
			boolean soulshot = skill.useSoulShot() && activeChar.isChargedShot(ShotType.SOULSHOTS);
			byte shield = Formulas.calcShldUse(activeChar, target, skill);
			double pAtk = activeChar.getPAtk(target);
			double damage = Formulas.calcBlowDamage(activeChar, target, skill, pAtk, shield, crit, soulshot, _power);
			
			if (crit)
			{
				damage *= 2;
			}
			
			// Manage attack or cast break of the target (calculating rate, sending message...)
			if (!target.isRaid() && Formulas.calcAtkBreak(target, damage))
			{
				target.breakAttack();
				target.breakCast();
			}
			
			if (damage > 0)
			{
				if (soulshot)
				{
					activeChar.setChargedShot(ShotType.SOULSHOTS, false);
				}
				
				if (!activeChar.isHpBlocked() && !target.isHpBlocked()) // No reflect/absorb if actor/target is invulnerable.
				{
					// Reduce HP of the target and calculate reflection damage to reduce HP of attacker if necessary (max reflect to 100%)
					double reflectPercent = Math.min(target.getStat().calcStat(Stats.REFLECT_DAMAGE_PERCENT, 0, null, null), 100);
					int reflectedDamage = 0;
					
					if (reflectPercent > 0)
					{
						reflectedDamage = (int) ((reflectPercent / 100) * damage);
						reflectedDamage = Math.min(reflectedDamage, reflectedDamage);
						activeChar.reduceCurrentHp(reflectedDamage, target, skill);
						activeChar.notifyDamageReceived(reflectedDamage, target, skill, crit, false, true);
					}
					
					// Absorb HP from the damage inflicted
					double absorbPercent = activeChar.getStat().calcStat(Stats.ABSORB_DAMAGE_PERCENT, 0);
					double absorbChance = activeChar.getStat().calcStat(Stats.ABSORB_DAMAGE_PERCENT_CHANCE, 0);
					
					if ((absorbPercent > 0) && (Rnd.get(100) < absorbChance))
					{
						int maxCanAbsorb = (int) (activeChar.getMaxRecoverableHp() - activeChar.getCurrentHp());
						int absorbDamage = (int) ((absorbPercent / 100) * damage);
						
						if (absorbDamage > maxCanAbsorb)
						{
							absorbDamage = maxCanAbsorb;
						}
						
						if (absorbDamage > 0)
						{
							activeChar.setCurrentHp(activeChar.getCurrentHp() + absorbDamage);
						}
					}
					
					// Absorb MP from the damage inflicted
					absorbPercent = activeChar.getStat().calcStat(Stats.ABSORB_MANA_DAMAGE_PERCENT, 0);
					
					if (absorbPercent > 0)
					{
						int maxCanAbsorb = (int) (activeChar.getMaxRecoverableMp() - activeChar.getCurrentMp());
						int absorbDamage = (int) ((absorbPercent / 100) * damage);
						
						if (absorbDamage > maxCanAbsorb)
						{
							absorbDamage = maxCanAbsorb;
						}
						
						if (absorbDamage > 0)
						{
							activeChar.setCurrentMp(activeChar.getCurrentMp() + absorbDamage);
						}
					}
				}
				
				activeChar.sendDamageMessage(target, (int) damage, false, crit, false);
				activeChar.sendPacket(Sound.SKILLSOUND_CRITICAL_HIT_2.getPacket());
				target.reduceCurrentHp(damage, activeChar, skill, crit);
				target.notifyDamageReceived(damage, activeChar, skill, crit, false, false);
			}
		}
	}
}