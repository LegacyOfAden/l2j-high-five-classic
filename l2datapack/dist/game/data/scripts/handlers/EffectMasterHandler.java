/*
 * Copyright (C) 2004-2018 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.l2jserver.gameserver.handler.EffectHandler;
import com.l2jserver.gameserver.model.effects.AbstractEffect;

import handlers.effecthandlers.consume.c_chameleon_rest;
import handlers.effecthandlers.consume.c_fake_death;
import handlers.effecthandlers.consume.c_hp;
import handlers.effecthandlers.consume.c_mp;
import handlers.effecthandlers.consume.c_mp_by_level;
import handlers.effecthandlers.consume.c_rest;
import handlers.effecthandlers.custom.i_bot_report;
import handlers.effecthandlers.custom.i_change_skill_level;
import handlers.effecthandlers.custom.i_fly_away;
import handlers.effecthandlers.custom.i_grow;
import handlers.effecthandlers.custom.i_unlock_chest;
import handlers.effecthandlers.custom.i_unlock_door;
import handlers.effecthandlers.custom.p_avoid_agro;
import handlers.effecthandlers.custom.p_block_act;
import handlers.effecthandlers.custom.p_block_attack;
import handlers.effecthandlers.custom.p_block_controll;
import handlers.effecthandlers.custom.p_block_move;
import handlers.effecthandlers.custom.p_block_skill_physical;
import handlers.effecthandlers.custom.p_block_spell;
import handlers.effecthandlers.custom.p_effect_buff;
import handlers.effecthandlers.custom.p_effect_debuff;
import handlers.effecthandlers.custom.p_violet_boy;
import handlers.effecthandlers.instant.i_add_hate;
import handlers.effecthandlers.instant.i_align_direction;
import handlers.effecthandlers.instant.i_backstab;
import handlers.effecthandlers.instant.i_betray;
import handlers.effecthandlers.instant.i_blink;
import handlers.effecthandlers.instant.i_bonuscount_up;
import handlers.effecthandlers.instant.i_call_party;
import handlers.effecthandlers.instant.i_call_pc;
import handlers.effecthandlers.instant.i_call_skill;
import handlers.effecthandlers.instant.i_capture_flag;
import handlers.effecthandlers.instant.i_capture_flag_start;
import handlers.effecthandlers.instant.i_capture_ownthing;
import handlers.effecthandlers.instant.i_change_face;
import handlers.effecthandlers.instant.i_change_hair_color;
import handlers.effecthandlers.instant.i_change_hair_style;
import handlers.effecthandlers.instant.i_confuse;
import handlers.effecthandlers.instant.i_consume_body;
import handlers.effecthandlers.instant.i_convert_item;
import handlers.effecthandlers.instant.i_cp;
import handlers.effecthandlers.instant.i_death;
import handlers.effecthandlers.instant.i_death_link;
import handlers.effecthandlers.instant.i_defuse_trap;
import handlers.effecthandlers.instant.i_delete_hate;
import handlers.effecthandlers.instant.i_delete_hate_of_me;
import handlers.effecthandlers.instant.i_detect_object;
import handlers.effecthandlers.instant.i_detect_trap;
import handlers.effecthandlers.instant.i_dispel_all;
import handlers.effecthandlers.instant.i_dispel_by_category;
import handlers.effecthandlers.instant.i_dispel_by_slot;
import handlers.effecthandlers.instant.i_dispel_by_slot_probability;
import handlers.effecthandlers.instant.i_energy_attack;
import handlers.effecthandlers.instant.i_escape;
import handlers.effecthandlers.instant.i_fatal_blow;
import handlers.effecthandlers.instant.i_fishing_cast;
import handlers.effecthandlers.instant.i_fishing_pumping;
import handlers.effecthandlers.instant.i_fishing_reeling;
import handlers.effecthandlers.instant.i_fly_self;
import handlers.effecthandlers.instant.i_focus_energy;
import handlers.effecthandlers.instant.i_focus_max_energy;
import handlers.effecthandlers.instant.i_focus_soul;
import handlers.effecthandlers.instant.i_food_for_pet;
import handlers.effecthandlers.instant.i_get_agro;
import handlers.effecthandlers.instant.i_harvesting;
import handlers.effecthandlers.instant.i_heal;
import handlers.effecthandlers.instant.i_holything_possess;
import handlers.effecthandlers.instant.i_hp;
import handlers.effecthandlers.instant.i_hp_by_level_self;
import handlers.effecthandlers.instant.i_hp_drain;
import handlers.effecthandlers.instant.i_hp_per_max;
import handlers.effecthandlers.instant.i_install_advance_base;
import handlers.effecthandlers.instant.i_install_camp;
import handlers.effecthandlers.instant.i_m_attack;
import handlers.effecthandlers.instant.i_m_attack_by_abnormal;
import handlers.effecthandlers.instant.i_m_attack_mp;
import handlers.effecthandlers.instant.i_m_soul_attack;
import handlers.effecthandlers.instant.i_mp;
import handlers.effecthandlers.instant.i_mp_by_level_self;
import handlers.effecthandlers.instant.i_mp_per_max;
import handlers.effecthandlers.instant.i_npc_kill;
import handlers.effecthandlers.instant.i_open_common_recipebook;
import handlers.effecthandlers.instant.i_open_dwarf_recipebook;
import handlers.effecthandlers.instant.i_p_attack;
import handlers.effecthandlers.instant.i_p_soul_attack;
import handlers.effecthandlers.instant.i_physical_attack_hp_link;
import handlers.effecthandlers.instant.i_pledge_send_system_message;
import handlers.effecthandlers.instant.i_randomize_hate;
import handlers.effecthandlers.instant.i_real_damage;
import handlers.effecthandlers.instant.i_rebalance_hp;
import handlers.effecthandlers.instant.i_refuel_airship;
import handlers.effecthandlers.instant.i_restoration;
import handlers.effecthandlers.instant.i_restoration_random;
import handlers.effecthandlers.instant.i_resurrection;
import handlers.effecthandlers.instant.i_run_away;
import handlers.effecthandlers.instant.i_set_skill;
import handlers.effecthandlers.instant.i_skill_turning;
import handlers.effecthandlers.instant.i_soul_blow;
import handlers.effecthandlers.instant.i_sowing;
import handlers.effecthandlers.instant.i_sp;
import handlers.effecthandlers.instant.i_spoil;
import handlers.effecthandlers.instant.i_steal_abnormal;
import handlers.effecthandlers.instant.i_summon;
import handlers.effecthandlers.instant.i_summon_agathion;
import handlers.effecthandlers.instant.i_summon_cubic;
import handlers.effecthandlers.instant.i_summon_npc;
import handlers.effecthandlers.instant.i_summon_pet;
import handlers.effecthandlers.instant.i_summon_trap;
import handlers.effecthandlers.instant.i_sweeper;
import handlers.effecthandlers.instant.i_target_cancel;
import handlers.effecthandlers.instant.i_teleport;
import handlers.effecthandlers.instant.i_teleport_to_target;
import handlers.effecthandlers.instant.i_transfer_hate;
import handlers.effecthandlers.instant.i_uninstall_advance_base;
import handlers.effecthandlers.instant.i_unsummon_agathion;
import handlers.effecthandlers.instant.i_vp_up;
import handlers.effecthandlers.instant.p_target_me_probability;
import handlers.effecthandlers.pump.p_ability_change;
import handlers.effecthandlers.pump.p_attack_attribute;
import handlers.effecthandlers.pump.p_attack_trait;
import handlers.effecthandlers.pump.p_avoid_skill;
import handlers.effecthandlers.pump.p_block_buff;
import handlers.effecthandlers.pump.p_block_buff_slot;
import handlers.effecthandlers.pump.p_block_chat;
import handlers.effecthandlers.pump.p_block_debuff;
import handlers.effecthandlers.pump.p_block_getdamage;
import handlers.effecthandlers.pump.p_block_party;
import handlers.effecthandlers.pump.p_block_resurrection;
import handlers.effecthandlers.pump.p_change_fishing_mastery;
import handlers.effecthandlers.pump.p_counter_skill;
import handlers.effecthandlers.pump.p_crystal_grade_modify;
import handlers.effecthandlers.pump.p_cubic_mastery;
import handlers.effecthandlers.pump.p_damage_shield;
import handlers.effecthandlers.pump.p_defence_attribute;
import handlers.effecthandlers.pump.p_defence_trait;
import handlers.effecthandlers.pump.p_disarm;
import handlers.effecthandlers.pump.p_expand_deco_slot;
import handlers.effecthandlers.pump.p_fatal_blow_rate;
import handlers.effecthandlers.pump.p_fear;
import handlers.effecthandlers.pump.p_hide;
import handlers.effecthandlers.pump.p_ignore_skill;
import handlers.effecthandlers.pump.p_limit_cp;
import handlers.effecthandlers.pump.p_limit_hp;
import handlers.effecthandlers.pump.p_limit_mp;
import handlers.effecthandlers.pump.p_luck;
import handlers.effecthandlers.pump.p_magic_mp_cost;
import handlers.effecthandlers.pump.p_max_cp;
import handlers.effecthandlers.pump.p_max_hp;
import handlers.effecthandlers.pump.p_max_mp;
import handlers.effecthandlers.pump.p_mp_shield;
import handlers.effecthandlers.pump.p_mp_vampiric_attack;
import handlers.effecthandlers.pump.p_passive;
import handlers.effecthandlers.pump.p_physical_polarm_target_single;
import handlers.effecthandlers.pump.p_pk_protect;
import handlers.effecthandlers.pump.p_preserve_abnormal;
import handlers.effecthandlers.pump.p_reduce_cancel;
import handlers.effecthandlers.pump.p_reflect_dd;
import handlers.effecthandlers.pump.p_reflect_skill;
import handlers.effecthandlers.pump.p_resist_abnormal_by_category;
import handlers.effecthandlers.pump.p_resist_dd_magic;
import handlers.effecthandlers.pump.p_resist_dispel_by_category;
import handlers.effecthandlers.pump.p_resurrection_special;
import handlers.effecthandlers.pump.p_reuse_delay;
import handlers.effecthandlers.pump.p_set_cloak_slot;
import handlers.effecthandlers.pump.p_soul_eating;
import handlers.effecthandlers.pump.p_speed;
import handlers.effecthandlers.pump.p_stat_up;
import handlers.effecthandlers.pump.p_target_me;
import handlers.effecthandlers.pump.p_transfer_damage_pc;
import handlers.effecthandlers.pump.p_transfer_damage_summon;
import handlers.effecthandlers.pump.p_transform;
import handlers.effecthandlers.pump.p_trigger_skill_by_attack;
import handlers.effecthandlers.pump.p_trigger_skill_by_avoid;
import handlers.effecthandlers.pump.p_trigger_skill_by_dmg;
import handlers.effecthandlers.pump.p_trigger_skill_by_skill;
import handlers.effecthandlers.pump.p_vampiric_attack;
import handlers.effecthandlers.ticks.t_hp;
import handlers.effecthandlers.ticks.t_hp_fatal;
import handlers.effecthandlers.ticks.t_mp;

/**
 * Effect Master handler.
 * @author BiggBoss, Zoey76
 */
public final class EffectMasterHandler
{
	private static final Logger _log = Logger.getLogger(EffectMasterHandler.class.getName());
	
	private static final Class<?>[] EFFECTS =
	{
		i_add_hate.class,
		i_align_direction.class,
		p_attack_trait.class,
		p_avoid_agro.class,
		i_backstab.class,
		i_betray.class,
		i_blink.class,
		p_block_act.class,
		p_block_attack.class,
		p_block_buff.class,
		p_block_chat.class,
		p_block_getdamage.class,
		p_block_debuff.class,
		p_block_move.class,
		p_block_party.class,
		p_block_buff_slot.class,
		p_block_skill_physical.class,
		p_block_spell.class,
		p_block_resurrection.class,
		i_bot_report.class,
		p_effect_buff.class,
		i_call_party.class,
		i_call_pc.class,
		i_call_skill.class,
		i_change_face.class,
		p_change_fishing_mastery.class,
		i_change_hair_color.class,
		i_change_hair_style.class,
		i_pledge_send_system_message.class,
		i_confuse.class,
		i_consume_body.class,
		c_chameleon_rest.class,
		c_fake_death.class,
		c_hp.class,
		c_mp.class,
		c_mp_by_level.class,
		c_rest.class,
		i_convert_item.class,
		i_cp.class,
		p_crystal_grade_modify.class,
		p_cubic_mastery.class,
		i_death_link.class,
		p_effect_debuff.class,
		p_defence_trait.class,
		i_delete_hate.class,
		i_delete_hate_of_me.class,
		i_detect_object.class,
		p_disarm.class,
		i_dispel_all.class,
		i_dispel_by_category.class,
		i_dispel_by_slot.class,
		i_dispel_by_slot_probability.class,
		p_set_cloak_slot.class,
		i_energy_attack.class,
		i_escape.class,
		i_fatal_blow.class,
		p_fear.class,
		i_fishing_cast.class,
		p_violet_boy.class,
		i_fly_self.class,
		i_focus_energy.class,
		i_focus_max_energy.class,
		i_focus_soul.class,
		i_food_for_pet.class,
		i_get_agro.class,
		i_bonuscount_up.class,
		i_sp.class,
		i_grow.class,
		i_harvesting.class,
		i_install_camp.class,
		i_heal.class,
		p_hide.class,
		p_reflect_skill.class,
		i_hp.class,
		i_hp_by_level_self.class,
		i_hp_drain.class,
		i_hp_per_max.class,
		i_death.class,
		p_luck.class,
		i_m_attack.class,
		i_m_attack_by_abnormal.class,
		i_m_attack_mp.class,
		i_m_soul_attack.class,
		i_mp_by_level_self.class,
		p_fatal_blow_rate.class,
		p_max_cp.class,
		p_max_hp.class,
		p_max_mp.class,
		i_mp.class,
		i_mp_per_max.class,
		p_preserve_abnormal.class,
		i_unlock_chest.class,
		i_npc_kill.class,
		i_open_common_recipebook.class,
		i_unlock_door.class,
		i_open_dwarf_recipebook.class,
		i_install_advance_base.class,
		i_uninstall_advance_base.class,
		p_passive.class,
		i_p_attack.class,
		i_physical_attack_hp_link.class,
		p_block_spell.class,
		i_p_soul_attack.class,
		i_fishing_pumping.class,
		p_pk_protect.class,
		i_randomize_hate.class,
		i_rebalance_hp.class,
		i_change_skill_level.class,
		i_fishing_reeling.class,
		i_refuel_airship.class,
		p_ignore_skill.class,
		p_avoid_skill.class,
		p_counter_skill.class,
		p_block_controll.class,
		i_restoration.class,
		i_restoration_random.class,
		p_reduce_cancel.class,
		p_resist_abnormal_by_category.class,
		p_resist_dispel_by_category.class,
		p_vampiric_attack.class,
		p_mp_shield.class,
		p_mp_vampiric_attack.class,
		p_resist_dd_magic.class,
		p_limit_hp.class,
		p_limit_mp.class,
		p_limit_cp.class,
		p_reflect_dd.class,
		p_damage_shield.class,
		p_defence_attribute.class,
		p_attack_attribute.class,
		p_stat_up.class,
		p_speed.class,
		i_resurrection.class,
		p_resurrection_special.class,
		i_run_away.class,
		p_ability_change.class,
		i_set_skill.class,
		p_physical_polarm_target_single.class,
		p_magic_mp_cost.class,
		p_reuse_delay.class,
		i_skill_turning.class,
		i_soul_blow.class,
		p_soul_eating.class,
		i_sowing.class,
		i_spoil.class,
		i_real_damage.class,
		i_steal_abnormal.class,
		i_summon.class,
		i_summon_agathion.class,
		i_summon_cubic.class,
		i_summon_npc.class,
		i_summon_pet.class,
		i_summon_trap.class,
		i_sweeper.class,
		i_holything_possess.class,
		i_capture_flag.class,
		i_capture_flag_start.class,
		i_capture_ownthing.class,
		p_expand_deco_slot.class,
		i_target_cancel.class,
		p_target_me.class,
		p_target_me_probability.class,
		i_teleport.class,
		i_teleport_to_target.class,
		i_fly_away.class,
		t_hp.class,
		t_hp_fatal.class,
		t_mp.class,
		p_transfer_damage_pc.class,
		p_transfer_damage_summon.class,
		i_transfer_hate.class,
		p_transform.class,
		i_detect_trap.class,
		i_defuse_trap.class,
		p_trigger_skill_by_attack.class,
		p_trigger_skill_by_avoid.class,
		p_trigger_skill_by_dmg.class,
		p_trigger_skill_by_skill.class,
		i_unsummon_agathion.class,
		i_vp_up.class,
	};
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args)
	{
		for (Class<?> c : EFFECTS)
		{
			if (c == null)
			{
				continue; // Disabled handler
			}
			EffectHandler.getInstance().registerHandler((Class<? extends AbstractEffect>) c);
		}
		
		// And lets try get size
		try
		{
			_log.log(Level.INFO, EffectMasterHandler.class.getSimpleName() + ": Loaded " + EffectHandler.getInstance().size() + " effect handlers.");
		}
		catch (Exception e)
		{
			_log.log(Level.WARNING, "Failed invoking size method for handler: " + EffectMasterHandler.class.getSimpleName(), e);
		}
	}
}