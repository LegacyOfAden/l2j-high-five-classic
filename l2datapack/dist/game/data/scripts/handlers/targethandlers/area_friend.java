/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.targethandlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.handler.ITargetTypeHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2SiegeFlagInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.skills.targets.L2TargetType;
import com.l2jserver.gameserver.model.zone.ZoneId;
import com.l2jserver.gameserver.util.Util;

/**
 * Area Friendly target handler implementation.
 * @author Adry_85, Zoey76
 */
public class area_friend implements ITargetTypeHandler
{
	private static final CharComparator CHAR_COMPARATOR = new CharComparator();
	
	@Override
	public L2Object[] getTargetList(Skill skill, L2Character activeChar, boolean onlyFirst, L2Character target)
	{
		List<L2Character> targetList = new ArrayList<>();
		
		if (target == null)
		{
			return empty_target_list;
		}
		
		final L2Character origin;
		final boolean srcInArena = (activeChar.isInsideZone(ZoneId.PVP) && !activeChar.isInsideZone(ZoneId.SIEGE));
		
		if (skill.getCastRange() >= 0)
		{
			if (onlyFirst)
			{
				return new L2Character[]
				{
					target
				};
			}
			
			origin = target;
			targetList.add(origin); // Add target to target list
		}
		else
		{
			origin = activeChar;
		}
		
		activeChar.setHeading(Util.calculateHeadingFrom(activeChar, target));
		
		int affectRange = skill.getFanRange() != null ? skill.getFanRange()[2] : skill.getAffectRange();
		final Collection<L2Character> objs = activeChar.getKnownList().getKnownCharactersInRadius(activeChar, affectRange);
		final int maxTargets = skill.getAffectLimit();
		for (L2Character obj : objs)
		{
			if (!checkTarget(activeChar, obj) || (obj == activeChar))
			{
				continue;
			}
			
			if (skill.getFanRange() != null ? skill.checkFan(activeChar, origin, obj, srcInArena) : skill.checkNormal(activeChar, origin, obj, srcInArena))
			{
				if ((maxTargets > 0) && (targetList.size() >= maxTargets))
				{
					break;
				}
				
				targetList.add(obj);
			}
		}
		
		// Sort creatures, the most injured first.
		Collections.sort(targetList, CHAR_COMPARATOR);
		
		if (targetList.isEmpty())
		{
			return empty_target_list;
		}
		return targetList.toArray(new L2Character[targetList.size()]);
	}
	
	private boolean checkTarget(L2Character activeChar, L2Character target)
	{
		if (!GeoData.getInstance().canSeeTarget(activeChar, target))
		{
			return false;
		}
		
		if ((target == null) || target.isAlikeDead() || target.isDoor() || (target instanceof L2SiegeFlagInstance) || target.isMonster())
		{
			return false;
		}
		
		if (target.isPlayable())
		{
			L2PcInstance actingPlayer = activeChar.getActingPlayer();
			L2PcInstance targetPlayer = target.getActingPlayer();
			
			if (activeChar == targetPlayer)
			{
				return true;
			}
			
			if ((actingPlayer == null) || (targetPlayer == null))
			{
				return false;
			}
			
			if (targetPlayer.inObserverMode() || targetPlayer.isInOlympiadMode())
			{
				return false;
			}
			
			if (((L2PcInstance) activeChar).isInDuelWith(target))
			{
				return false;
			}
			
			if (((L2PcInstance) activeChar).isInPartyWith(target))
			{
				return true;
			}
			
			if (!target.isAutoAttackable(actingPlayer))
			{
				return false;
			}
			
			// Only siege allies.
			if (((L2PcInstance) activeChar).isInSiege() && !((L2PcInstance) activeChar).isOnSameSiegeSideWith(targetPlayer))
			{
				return false;
			}
			
			if (target.isInsideZone(ZoneId.PVP))
			{
				return false;
			}
			
			if (((L2PcInstance) activeChar).isInClanWith(target) || ((L2PcInstance) activeChar).isInAllyWith(target) || ((L2PcInstance) activeChar).isInCommandChannelWith(target))
			{
				return true;
			}
			
			if (((L2PcInstance) activeChar).isAtWarWith(target))
			{
				return false;
			}
			
			if ((targetPlayer.getPvpFlag() > 0) || (targetPlayer.getKarma() > 0))
			{
				return false;
			}
		}
		return true;
	}
	
	public static class CharComparator implements Comparator<L2Character>
	{
		@Override
		public int compare(L2Character char1, L2Character char2)
		{
			return Double.compare((char1.getCurrentHp() / char1.getMaxHp()), (char2.getCurrentHp() / char2.getMaxHp()));
		}
	}
	
	@Override
	public Enum<L2TargetType> getTargetType()
	{
		return L2TargetType.area_friend;
	}
}