/*
 * Copyright (C) 2004-2017 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.targethandlers;

import static com.l2jserver.gameserver.network.SystemMessageId.INVALID_TARGET;

import com.l2jserver.gameserver.handler.ITargetTypeHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.skills.targets.L2TargetType;
import com.l2jserver.gameserver.model.zone.ZoneId;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Enemy target type handler.
 * @author Zoey76
 * @since 2.6.0.0
 */
public class enemy implements ITargetTypeHandler
{
	@Override
	public L2Object[] getTargetList(Skill skill, L2Character activeChar, boolean onlyFirst, L2Character target)
	{
		switch (skill.getAffectScope())
		{
			case single:
			{
				final L2PcInstance player = activeChar.getActingPlayer();
				if (target == null)
				{
					return empty_target_list;
				}
				
				if ((target == activeChar) || (target == player))
				{
					activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
					return empty_target_list;
				}
				
				if (!target.isAttackable() && //
					(player != null) && //
					player.isInPartyWith(target) && //
					player.isInClanWith(target) && //
					player.isInAllyWith(target) && //
					player.isInCommandChannelWith(target) && //
					player.isOnSameSiegeSideWith(target) && //
					!(player.isInsideZone(ZoneId.PVP) && target.isInsideZone(ZoneId.PVP)) && //
					!player.isInOlympiadMode() && //
					!player.isAtWarWith(target) && //
					!player.checkIfPvP(target) && //
					!player.getCurrentSkill().isCtrlPressed())
				{
					activeChar.sendPacket(INVALID_TARGET);
					return empty_target_list;
				}
				
				return new L2Character[]
				{
					target
				};
			}
		}
		return empty_target_list;
	}
	
	@Override
	public Enum<L2TargetType> getTargetType()
	{
		return L2TargetType.enemy;
	}
}
