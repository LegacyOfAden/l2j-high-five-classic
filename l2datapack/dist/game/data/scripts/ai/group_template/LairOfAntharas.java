/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.group_template;

import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.NpcStringId;
import com.l2jserver.gameserver.network.clientpackets.Say2;
import com.l2jserver.gameserver.network.serverpackets.ValidateLocation;
import com.l2jserver.gameserver.util.Util;

import ai.npc.AbstractNpcAI;

/**
 * Lair of Antharas AI.
 * @author St3eT, UnAfraid, Maneco2
 */
public final class LairOfAntharas extends AbstractNpcAI
{
	// NPC
	final private static int DRAGON_KNIGHT = 22844;
	final private static int DRAGON_KNIGHT2 = 22845;
	final private static int ELITE_DRAGON_KNIGHT = 22846;
	final private static int DRAGON_KNIGHT_WARRIOR = 22847;
	final private static int DRAGON_GUARD = 22852;
	final private static int DRAGON_MAGE = 22853;
	
	private static final int[] SPAWN_ANIMATION =
	{
		22844, // Dragon Knight
		22847, // Dragon Knight Warrior
	};
	
	private LairOfAntharas()
	{
		super(LairOfAntharas.class.getSimpleName(), "ai/group_template");
		addKillId(DRAGON_KNIGHT, DRAGON_KNIGHT2, DRAGON_GUARD, DRAGON_MAGE);
		addSpawnId(DRAGON_KNIGHT, DRAGON_KNIGHT2, ELITE_DRAGON_KNIGHT, DRAGON_KNIGHT_WARRIOR, DRAGON_GUARD, DRAGON_MAGE);
		addMoveFinishedId(DRAGON_GUARD, DRAGON_MAGE);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.equals("CHECK_HOME") && (npc != null) && !npc.isDead())
		{
			if ((npc.calculateDistance(npc.getSpawn().getLocation(), false, false) > 10) && !npc.isInCombat())
			{
				((L2Attackable) npc).returnHome();
			}
			else if ((npc.getHeading() != npc.getSpawn().getHeading()) && !npc.isInCombat())
			{
				npc.setHeading(npc.getSpawn().getHeading());
				npc.broadcastPacket(new ValidateLocation(npc));
			}
		}
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		switch (npc.getId())
		{
			case DRAGON_KNIGHT:
			{
				if (getRandom(100) < 15)
				{
					final L2Npc newKnight = addSpawn(DRAGON_KNIGHT2, npc, false, 0, true);
					broadcastNpcSay(newKnight, Say2.NPC_SHOUT, NpcStringId.THOSE_WHO_SET_FOOT_IN_THIS_PLACE_SHALL_NOT_LEAVE_ALIVE);
					npc.deleteMe();
				}
				break;
			}
			case DRAGON_KNIGHT2:
			{
				if (getRandom(100) < 15)
				{
					final L2Npc eliteKnight = addSpawn(ELITE_DRAGON_KNIGHT, npc, false, 0, true);
					broadcastNpcSay(eliteKnight, Say2.NPC_SHOUT, NpcStringId.IF_YOU_WISH_TO_SEE_HELL_I_WILL_GRANT_YOU_YOUR_WISH);
					npc.deleteMe();
				}
				break;
			}
			case DRAGON_GUARD:
			case DRAGON_MAGE:
			{
				cancelQuestTimer("CHECK_HOME", npc, null);
				break;
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		final L2Attackable mob = (L2Attackable) npc;
		mob.setOnKillDelay(3000);
		if (npc.getId() == DRAGON_KNIGHT)
		{
			final int newZ = npc.getZ() + 100;
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY(), newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY() + 20, newZ, 0, false, 0);
		}
		else if (npc.getId() == DRAGON_KNIGHT2)
		{
			final int newZ = npc.getZ() + 100;
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY(), newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY() - 20, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY() + 20, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 30, npc.getY() + 40, newZ, 0, false, 0);
		}
		else if (npc.getId() == ELITE_DRAGON_KNIGHT)
		{
			final int newZ = npc.getZ() + 100;
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 90, npc.getY(), newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() + 90, npc.getY(), newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX(), npc.getY() - 90, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX(), npc.getY() + 90, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() + 60, npc.getY() + 60, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 60, npc.getY() - 60, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() + 60, npc.getY() - 60, newZ, 0, false, 0);
			addSpawn(DRAGON_KNIGHT_WARRIOR, npc.getX() - 60, npc.getY() + 60, newZ, 0, false, 0);
		}
		else if ((npc.getId() == DRAGON_GUARD) || (npc.getId() == DRAGON_MAGE))
		{
			mob.setIsNoRndWalk(true);
			startQuestTimer("CHECK_HOME", 10000, npc, null, true);
		}
		if (Util.contains(SPAWN_ANIMATION, npc.getId()))
		{
			npc.setShowSummonAnimation(true);
		}
		return super.onSpawn(npc);
	}
	
	public static void main(String[] args)
	{
		new LairOfAntharas();
	}
}