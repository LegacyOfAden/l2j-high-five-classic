/*
 * Copyright (C) 2004-2015 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.individual;

import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.NpcStringId;

import ai.npc.AbstractNpcAI;

/**
 * Blade Otis AI.
 * @author Zealar
 */
public final class BladeOtis extends AbstractNpcAI
{
	// NPCs
	private static final int BLADE_OTIS = 18562;
	private static final int FOLLOWER_OF_OTIS = 18563;
	
	private final Myself myself = new Myself();
	
	private class Myself
	{
		Myself()
		{
			
		}
		
		public int i_ai1;
		public int i_ai2;
		public int i_ai3;
		public L2Npc sm;
	}
	
	private BladeOtis()
	{
		super(BladeOtis.class.getSimpleName(), "ai/kamaloka");
		addAttackId(BLADE_OTIS);
		addSpawnId(BLADE_OTIS);
		addKillId(BLADE_OTIS);
		addKillId(FOLLOWER_OF_OTIS);
		myself.i_ai1 = 0;
		myself.i_ai2 = 0;
		myself.i_ai3 = 0;
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.equals("9008"))
		{
			if ((myself.i_ai2 == 1) && (myself.i_ai3 < 3))
			{
				addSpawn(FOLLOWER_OF_OTIS, new Location(myself.sm.getX() + 10, myself.sm.getY() + 10, myself.sm.getZ()), false, 0);
				myself.i_ai3 += 1;
			}
			startQuestTimer("9008", 30000, null, null);
		}
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isSummon)
	{
		if ((((npc.getCurrentHp() / npc.getMaxHp()) * 100) <= 60) && (myself.i_ai1 == 0))
		{
			
			addSpawn(FOLLOWER_OF_OTIS, new Location(myself.sm.getX() + 10, myself.sm.getY() + 10, myself.sm.getZ()), false, 0);
			myself.i_ai1 = 1;
			startQuestTimer("9008", 30000, null, null);
		}
		
		if ((((npc.getCurrentHp() / npc.getMaxHp()) * 100) <= 30) && (myself.i_ai2 == 0))
		{
			myself.i_ai2 = 1;
		}
		return super.onAttack(npc, attacker, damage, isSummon);
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		myself.sm = npc;
		return super.onSpawn(npc);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		switch (npc.getId())
		{
			case BLADE_OTIS:
			{
				break;
			}
			case FOLLOWER_OF_OTIS:
			{
				broadcastNpcSay(myself.sm, 1, NpcStringId.IF_YOU_THOUGHT_THAT_MY_SUBORDINATES_WOULD_BE_SO_FEW_YOU_ARE_MISTAKEN);
				addSpawn(FOLLOWER_OF_OTIS, new Location(myself.sm.getX() + 10, myself.sm.getY() + 10, myself.sm.getZ()), false, 0);
				break;
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
	
	public static void main(String[] args)
	{
		new BladeOtis();
	}
}